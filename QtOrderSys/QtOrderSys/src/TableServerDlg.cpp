﻿#include "TableServerDlg.h"
#include "qboxlayout.h"
#include "qformlayout.h"
#include "qbitmap.h"
#include "OrderDlg.h"
#include "qmessagebox.h"
#include <sstream>

using namespace N_ORDERSYS;


TableServerDlg::TableServerDlg(TableManager::ptr ptr)
	:mTableManagerPtr(ptr)
{
	initFB();
	initValue();

	//开台点菜按钮事件
	connect(mBtnOpenTable, &QPushButton::clicked, this,&TableServerDlg::onBtnOpenTable);
	//点菜结束按钮事件
	connect(mBtnOrderOver, &QPushButton::clicked, this, &TableServerDlg::onBtnOrderOver);
	//开始上菜按钮事件
	connect(mBtnBeginUp, &QPushButton::clicked, this, &TableServerDlg::onBtnUpCook);
	//上菜结束按钮
	connect(mBtnOverUp, &QPushButton::clicked, this, &TableServerDlg::onBtnOverUp);
	//结账按钮
	connect(mBtnSettle, &QPushButton::clicked, this, &TableServerDlg::onBtnSettle);
	//清理按钮
	connect(mBtnClean, &QPushButton::clicked, this, &TableServerDlg::onBtnClean);

	connect(mBtnExit, &QPushButton::clicked, this,&QDialog::accept);
}


TableServerDlg::~TableServerDlg()
{
}

void N_ORDERSYS::TableServerDlg::initFB()
{
	if (this->objectName().isEmpty())
		this->setObjectName(QString::fromUtf8("TableServer"));
	this->resize(850, 460);

	auto verticalLayout_2 = new QVBoxLayout(this);
	auto horizontalLayout_3 = new QHBoxLayout();
	auto verticalLayout = new QVBoxLayout();

	auto horizontalLayout = new QHBoxLayout();
	//horizontalLayout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
#define CREATESTATEICON(NAME,ICONPATH)\
	mLabel##NAME = new QLabel(this);\
	mLabel##NAME->setMaximumSize(QSize(60, 60));\
	mLabel##NAME->setPixmap(QPixmap(QString::fromUtf8(ICONPATH)));\
	mLabel##NAME->setScaledContents(true);\
	horizontalLayout->addWidget(mLabel##NAME);

	CREATESTATEICON(TS_OK, ":/QtOrderSys/res/icon/iconTSoking.png");
	CREATESTATEICON(TS_ORDERING, ":/QtOrderSys/res/icon/iconTSordering.png");
	CREATESTATEICON(TS_COOKING, ":/QtOrderSys/res/icon/iconTScooking.png");
	CREATESTATEICON(TS_UPING, ":/QtOrderSys/res/icon/iconTSuping.png");
	CREATESTATEICON(TS_SERVING, ":/QtOrderSys/res/icon/iconTSserving.png");
	CREATESTATEICON(TS_CLEANING, ":/QtOrderSys/res/icon/iconTScleaning.png");

#undef CREATESTATEICON
	//horizontalLayout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
	verticalLayout->addLayout(horizontalLayout);


	auto horizontalLayout_2 = new QHBoxLayout();

	auto verticalLayout_TableIcon = new QVBoxLayout();

	mCustomInfo = new QLabel(QString::fromLocal8Bit("客户相关的信息客户相关的信息客户相关的信息客户相关的信息客户相关的信息客户相关的信息"),this);
	mCustomInfo->setWordWrap(true);
	mCustomInfo->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignBottom);
	verticalLayout_TableIcon->addWidget(mCustomInfo);


	mTableIcon = new QLabel(this);
	mTableIcon->setAlignment(Qt::AlignCenter);
	mTableIcon->setPixmap(QPixmap(QString::fromUtf8(":/QtOrderSys/res/icon/iconOpenTable.png")));
	verticalLayout_TableIcon->addWidget(mTableIcon);

	horizontalLayout_2->addLayout(verticalLayout_TableIcon);

	mServerLog = new QTextBrowser(this);
	mServerLog->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
	horizontalLayout_2->addWidget(mServerLog);

	verticalLayout->addLayout(horizontalLayout_2);
	horizontalLayout_3->addLayout(verticalLayout);

	auto formLayout = new QGridLayout();

#define CREATEBTN(BTNNAME,TEXT,NO)\
	BTNNAME = new QPushButton(this);\
	BTNNAME->setObjectName(QString::fromUtf8(#BTNNAME));\
	BTNNAME->setMinimumSize(QSize(100, 100));\
	BTNNAME->setText(QString::fromLocal8Bit(TEXT));\
	formLayout->addWidget(BTNNAME, NO/2, NO%2, 1, 1);

	CREATEBTN(mBtnOpenTable, "开台点菜",0);
	CREATEBTN(mBtnOrderOver, "点菜完毕",1);
	CREATEBTN(mBtnBeginUp, "开始上菜",2);
	CREATEBTN(mBtnOverUp, "结束上菜",3);
	CREATEBTN(mBtnSettle, "结账",4);
	CREATEBTN(mBtnClean, "清理",5);
	CREATEBTN(mBtnAdd_Remove, "加减菜",6);
	CREATEBTN(mBtnExit, "退出",7);

#undef CREATEBTN
	horizontalLayout_3->addLayout(formLayout);
	verticalLayout_2->addLayout(horizontalLayout_3);
	verticalLayout_2->addItem(new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed));
}

void N_ORDERSYS::TableServerDlg::initValue()
{
	//按鈕只能在當前狀態下可以使用
	//mTableManagerPtr->getStateNow();
	reflushDlg();
	
}

void N_ORDERSYS::TableServerDlg::reflushDlg()
{
	reflushTableIcon();
	reflushBtn();
	reflushStateIcon();
	reflushServerLogCtrl();
}

void N_ORDERSYS::TableServerDlg::reflushBtn()
{
	auto stateNow = mTableManagerPtr->getStateNow();

	switch (stateNow)
	{
	case N_ORDERSYS::TS_OK:
		mBtnOpenTable->setEnabled(true);
		mBtnOrderOver->setEnabled(false);
		mBtnBeginUp->setEnabled(false);
		mBtnOverUp->setEnabled(false);
		mBtnSettle->setEnabled(false);
		mBtnClean->setEnabled(false);
		mBtnAdd_Remove->setEnabled(false);
		break;
	case N_ORDERSYS::TS_ORDERING:
		mBtnOpenTable->setEnabled(true);
		mBtnOrderOver->setEnabled(true);
		mBtnBeginUp->setEnabled(false);
		mBtnOverUp->setEnabled(false);
		mBtnSettle->setEnabled(false);
		mBtnClean->setEnabled(false);
		mBtnAdd_Remove->setEnabled(false);
		break;
	case N_ORDERSYS::TS_COOKING:
		mBtnOpenTable->setEnabled(false);
		mBtnOrderOver->setEnabled(false);
		mBtnBeginUp->setEnabled(true);
		mBtnOverUp->setEnabled(false);
		mBtnSettle->setEnabled(false);
		mBtnClean->setEnabled(false);
		mBtnAdd_Remove->setEnabled(true);
		break;
	case N_ORDERSYS::TS_UPING:
		mBtnOpenTable->setEnabled(false);
		mBtnOrderOver->setEnabled(false);
		mBtnBeginUp->setEnabled(true);
		mBtnOverUp->setEnabled(true);
		mBtnSettle->setEnabled(false);
		mBtnClean->setEnabled(false);
		mBtnAdd_Remove->setEnabled(true);
		break;
	case N_ORDERSYS::TS_SERVING:
		mBtnOpenTable->setEnabled(false);
		mBtnOrderOver->setEnabled(false);
		mBtnBeginUp->setEnabled(false);
		mBtnOverUp->setEnabled(false);
		mBtnSettle->setEnabled(true);
		mBtnClean->setEnabled(false);
		mBtnAdd_Remove->setEnabled(true);
		break;
	case N_ORDERSYS::TS_CLEANING:
		mBtnOpenTable->setEnabled(false);
		mBtnOrderOver->setEnabled(false);
		mBtnBeginUp->setEnabled(false);
		mBtnOverUp->setEnabled(false);
		mBtnSettle->setEnabled(false);
		mBtnClean->setEnabled(true);
		mBtnAdd_Remove->setEnabled(false);
		break;
	default:
		assert(false);
		break;
	}
}

void N_ORDERSYS::TableServerDlg::reflushTableIcon()
{
	auto color = N_ORDERSYS::TableStateMap::instance().getTableColor(mTableManagerPtr->getStateNow());
	QString colorStr = "background-color: " + QColor(color).name();
	mTableIcon->setStyleSheet(colorStr);
}

void N_ORDERSYS::TableServerDlg::reflushStateIcon()
{
	QString unSelctColorStr = "";

	auto color = N_ORDERSYS::TableStateMap::instance().getTableColor(mTableManagerPtr->getStateNow());
	QString selectColorStr = "background-color: " + QColor(color).name();

#define SETLABELBACKCOLOR(NAME)\
	mLabel##NAME->setStyleSheet(unSelctColorStr);\
	if(NAME == mTableManagerPtr->getStateNow())\
		mLabel##NAME->setStyleSheet(selectColorStr);\
	
	SETLABELBACKCOLOR(TS_OK);
	SETLABELBACKCOLOR(TS_ORDERING);
	SETLABELBACKCOLOR(TS_COOKING);
	SETLABELBACKCOLOR(TS_UPING);
	SETLABELBACKCOLOR(TS_SERVING);
	SETLABELBACKCOLOR(TS_CLEANING);

#undef SETLABELBACKCOLOR
}

void N_ORDERSYS::TableServerDlg::reflushServerLogCtrl()
{
	mServerLog->clear();

	if (mTableManagerPtr != nullptr) {
		auto ss = mTableManagerPtr->getBillingDetails()->getServerLog_u8();
		this->append2ServerLog(ss);
	}
}

void N_ORDERSYS::TableServerDlg::append2ServerLog(const std::string & info)
{
	mServerLog->append(QString::fromUtf8((info.c_str())));
}

void N_ORDERSYS::TableServerDlg::onBtnOpenTable()
{
	//获取当前订单，订单有内容，根据内容初始化弹出的点菜对话框
	auto orderlst = mTableManagerPtr->getBillingDetails()->getOrderDishList();
	auto  supportLst = SupportDishList::instance().getSupportDishListCopy();
	OrderDishDlg dlg(supportLst, orderlst);
	if (QDialog::Accepted != dlg.exec()) {
		return;
	}

	//保存订单
	if (dlg.getOrderDishList() != *orderlst) {
		auto ss = dlg.getOrderDishList().getOrderListStr_u8();
		//输出点菜信息
		SupportDishList::instance().setSupportDishList(dlg.getSupportDishList());
		mTableManagerPtr->getBillingDetails()->setOrderDishList(dlg.getOrderDishList());
		mTableManagerPtr->getBillingDetails()->appendServerLog_u8(ss);
		this->append2ServerLog(ss);
	}

	if (mTableManagerPtr->getTableStateContext()->getStateNow() != EN_TableState::TS_ORDERING) {
		mTableManagerPtr->getTableStateContext()->next();
		reflushDlg();
	}
}

void N_ORDERSYS::TableServerDlg::onBtnOrderOver()
{
	//为空不能点击
	auto orderlst = mTableManagerPtr->getBillingDetails()->getOrderDishList();
	if (orderlst->isEmpty() == true) {
		QMessageBox::information(this, "tips", QString::fromLocal8Bit("还没有点菜呢"));
		return;
	}
	//提醒
	if (QMessageBox::question(this, "tips", QString::fromLocal8Bit("点菜确定了吗")) != QMessageBox::StandardButton::Yes) {
		return;
	}
	//切换
	mTableManagerPtr->getTableStateContext()->next();
	reflushDlg();
}

void N_ORDERSYS::TableServerDlg::onBtnUpCook()
{
	//弹出对话框，左边是已经点的才，右边有已经上的菜，可以向右，不能向左。
	auto orderlst = mTableManagerPtr->getBillingDetails()->getOrderDishList();
	auto hadUplst = mTableManagerPtr->getBillingDetails()->getHadUpDishList();
	//已点-已上等于左边的菜单。
	auto notUplst = *orderlst - *hadUplst;
	UpDishDlg dlg(notUplst, *hadUplst);
	if (QDialog::Accepted != dlg.exec()) {
		return;
	}

	//有改变，改为上菜阶段，
	if (mTableManagerPtr->getTableStateContext()->getStateNow() != EN_TableState::TS_UPING) {
		mTableManagerPtr->getTableStateContext()->next();
		reflushDlg();
	}

	//保存订单，有菜上之后
	if (dlg.getHadupDishList() != *hadUplst) {
		auto ss = dlg.getHadupDishList().getOrderListStr_u8();

		mTableManagerPtr->getBillingDetails()->setHadUpDishList(dlg.getHadupDishList());
		mTableManagerPtr->getBillingDetails()->appendServerLog_u8(ss);
		this->append2ServerLog(ss);
	}
	//全上
	if (*orderlst == *hadUplst) {
		QMessageBox::information(this, "tips", QString::fromLocal8Bit("菜已经上齐，请提醒客户"));
	}
}

void N_ORDERSYS::TableServerDlg::onBtnOverUp()
{
	auto orderlst = mTableManagerPtr->getBillingDetails()->getOrderDishList();
	auto hadUplst = mTableManagerPtr->getBillingDetails()->getHadUpDishList();
	if (*orderlst != *hadUplst) {
		QMessageBox::information(this, "tips", QString::fromLocal8Bit("菜还没有上齐，请确认"));
		return;
	}
	//切换
	mTableManagerPtr->getTableStateContext()->next();
	reflushDlg();
}

void N_ORDERSYS::TableServerDlg::onBtnSettle()
{
	//获得账单
	auto orderLst = mTableManagerPtr->getBillingDetails()->getOrderDishList();
	auto upLst = mTableManagerPtr->getBillingDetails()->getHadUpDishList();
	if (*orderLst != *upLst) {
		if (QMessageBox::question(this, "tips", QString::fromLocal8Bit("菜没有上齐，是否结账")) != QMessageBox::StandardButton::Yes) {
			return;
		}
	}
	//显示账单对话框，包括客户信息，菜单信息，汇总信息，日期时间，桌号等
	std::ostringstream ss;
	ss << mTableManagerPtr->getCustomInfo()->getPlayStr() << std::endl;
	auto his=mTableManagerPtr->getTableStateContext()->getHisTableState();
	for (auto it : his) {
		auto tt = std::chrono::system_clock::to_time_t(it.first);
		std::string st =ctime(&tt);
		st = st.substr(0, st.size() - 1);//去除ctime导致的换行，其实自定义输出更合适
		ss << st << ":" << it.second->getStateNowStr() << std::endl;
	}
	


	QMessageBox::information(this, "tips", QString::fromLocal8Bit(ss.str().c_str())+ QString::fromUtf8(orderLst->getOrderListStr_u8().c_str()));

	//确认账单
	//结账结束
	//将数据写入到数据库中
	mTableManagerPtr->getBillingDetails()->to_DB();

	//进入下一步
	mTableManagerPtr->getTableStateContext()->next();
	reflushDlg();
}

void N_ORDERSYS::TableServerDlg::onBtnClean()
{
	QMessageBox::information(this, "tips", QString::fromLocal8Bit("桌面清理完毕"));
	//进入下一步
	mTableManagerPtr->reset();
	reflushDlg();
}
