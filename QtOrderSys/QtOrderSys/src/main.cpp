﻿/**
*	@file main.cpp是整个程序的入口
*	@brief 程序入口函数main
*
*	依次完成全局变量全局对象的初始化
*	用户登录窗口弹出
*	判断用户登录状态，确定是否进入主窗口
*	最后等待用户主窗口的退出
*/


#include "QtOrderSys.h"
#include <QtWidgets/QApplication>

#include "AppContext.h"
#include "LoginDlg.h"
#include "databaseOperatoe.h"
#include "LoginUserLmt.h"
#include "TableState.h"
#include "log.h"

#include <iostream>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	//日志功能初始化
	NWENDY_LOG_INIT(Nwendy::LogLevel::LL_ALL, NWENDY_FILEAPPEND("./log"), 100,10);
	//程序配置模块初始化
	N_ORDERSYS::AppContext::instance().init();
	//数据库模块初始化
	if (N_ORDERSYS::DataBaseOperator::instance().initDataBase() == false) {
		OLOG_E() << "数据库初始化失败";
		return 1;
	}
	N_ORDERSYS::AppContext::instance().initAfterDb();
	
	//登录
	//N_ORDERSYS::LoginDlg mLoginDlg{ nullptr };
	//auto ret=mLoginDlg.exec();
	//if (ret == QDialog::Rejected) {
	//	return 0;
	//}
	//OLOG_D() << "用户登录成功";

	N_ORDERSYS::TableStateMap::instance().init();

	N_ORDERSYS::QtOrderSys mMainWnd;
	mMainWnd.show();

    return a.exec();
}
