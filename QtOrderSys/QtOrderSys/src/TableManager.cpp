﻿#include "TableManager.h"
#include "qmessagebox.h"
using namespace N_ORDERSYS;


TableManager::TableManager()
{
	mTableStatePtr = std::make_shared<TableStateContext>(std::make_shared<TableState_ok>());
	mCustomInfo = std::make_shared<CustomInfo>();
	mBillingDetails = std::make_shared<BillingDetails>();
}

N_ORDERSYS::TableManager::TableManager(int tableID, TableMounment tableMounment)
	:mTableID(tableID),mTableMounment(tableMounment)
{
	mTableStatePtr = std::make_shared<TableStateContext>( std::make_shared<TableState_ok>());
	mCustomInfo = std::make_shared<CustomInfo>();
	mBillingDetails = std::make_shared<BillingDetails>();
}


TableManager::~TableManager()
{
}

void N_ORDERSYS::TableManager::reset()
{
	mTableStatePtr = std::make_shared<TableStateContext>(std::make_shared<TableState_ok>());
	mCustomInfo = std::make_shared<CustomInfo>();
	mBillingDetails = std::make_shared<BillingDetails>();
}

EN_TableState N_ORDERSYS::TableManager::getStateNow() const
{
	return mTableStatePtr->getStateNow();
}