﻿#include "LoginUserDbOperator.h"
#include "log.h"

N_ORDERSYS::LoginUserDbOperator::LoginUserDbOperator(const std::shared_ptr<PGconn>& conn)
{
	mDBconnect = conn;
	AppContext::instance().query("USERTABLENAME", mUserTableName);
}

bool N_ORDERSYS::LoginUserDbOperator::hasUser(const std::string taccount)
{
	if (mDBconnect== nullptr) {
		OLOG_E() << "数据库连接失败，可能没有数据库或者没有初始化数据库的调用";
		return false;
	}

	std::string sql = "SELECT * FROM \"" + mUserTableName + "\" WHERE account = '" + taccount + "'";
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_TUPLES_OK != PQresultStatus(ret)) {
		PQclear(ret);
		OLOG_W() << "从数据库查询用户时失败";
		return false;
	}
	if (PQntuples(ret) == 0) {
		PQclear(ret);
		OLOG_W() << "数据库中没有用户：" << taccount;
		return false;
	}
	PQclear(ret);
	OLOG_D() << "数据库中查询到用户：" << taccount;
	return true;
}

/**
*	@param	tAccount	用户名
*	@param	pwd	用户密码
*	@return	true表示存在用户且密码正确
*/
bool N_ORDERSYS::LoginUserDbOperator::isRightpwd(const std::string taccount, const std::string & pwd)
{
	if (hasUser(taccount) == false)
		return false;
	std::string sql = "SELECT pwd FROM \"" + mUserTableName + "\" WHERE account = '" + taccount + "'";
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_TUPLES_OK != PQresultStatus(ret)) {
		PQclear(ret);
		return false;
	}
	//1行都没有表示没有查询到，由于数据库的设计不会大于1；
	//由于先判断有没有用户，所以这里一定是返回1
	if (PQntuples(ret) != 1) {
		PQclear(ret);
		return false;
	}
	std::string pwdFromDB = PQgetvalue(ret, 0, 0);
	if (pwdFromDB != pwd.c_str()) {
		PQclear(ret);
		OLOG_W() << "数据库中用户：\"" << taccount << "\"的密码和输入不符";
		return false;
	}
	PQclear(ret);
	OLOG_D() << "数据库中用户：\"" << taccount << "\"的密码和输入一致";
	return true;
}

bool N_ORDERSYS::LoginUserDbOperator::hasLimit(const std::string taccount)
{
	return false;
}

bool N_ORDERSYS::LoginUserDbOperator::addStaff(const LoginUser & usr)
{
	//INSERT INTO "tAccount" VALUES('admin','张三','Password123','18273111773','',31,1,0);
	if (mDBconnect == nullptr)
		return false;
	std::string sql = std::string("INSERT INTO ") + "\"" + mUserTableName + "\"" + " VALUES " + "(" + usr.toDBstr() + ");";
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	auto code = PQresultStatus(ret);
	if (PGRES_COMMAND_OK != code) {
		PQclear(ret);
		OLOG_W() << "向数据库中添加用户：" << usr.getAccount() << "失败";
		return false;
	}

	PQclear(ret);
	OLOG_D() << "向数据库中添加用户：" << usr.getAccount();
	return true;
}

bool N_ORDERSYS::LoginUserDbOperator::alterStaff(const LoginUser & usr)
{
	//简化成先删除后增添
	//if (mConn == nullptr)
	//	return false;
	//std::string sql = "UPDATE "+'\"' + mUserTableName + '\"'+" SET  "+"(" + usr.toDBstr + ") "+" WHERE " + "account=" +"'"+ usr.getAccount() + "'";
	//PGresult * ret = PQexec(mConn, sql.c_str());
	//if (PGRES_COMMAND_OK != PQresultStatus(ret)) {
	//	PQclear(ret);
	//	return false;
	//}

	//PQclear(ret);
	//return true;

	if (hasUser(usr.getAccount()) == false)
		return false;
	PQexec(mDBconnect.get(), "BEGIN");
	if (removeStaff(usr) && addStaff(usr)) {
		PQexec(mDBconnect.get(), "COMMIT");
		OLOG_D() << "向数据库中修改用户：" << usr.getAccount();
		return true;
	}
	else {
		PQexec(mDBconnect.get(), "ROLLBACK");
		OLOG_W() << "向数据库中修改用户：" << usr.getAccount() << "失败";
		return false;
	}
}

bool N_ORDERSYS::LoginUserDbOperator::removeStaff(const LoginUser & usr)
{
	return removeStaff(usr.getAccount());
}

bool N_ORDERSYS::LoginUserDbOperator::removeStaff(const std::string & usrAccount)
{
	if (hasUser(usrAccount) == false)
		return false;

	//DELETE FROM "tAccount" WHERE "account"='admin';
	std::string sql = std::string("DELETE FROM ") + "\"" + mUserTableName + "\"" + " WHERE " + "account=" + "'" + usrAccount + "'";
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_COMMAND_OK != PQresultStatus(ret)) {
		PQclear(ret);
		OLOG_W() << "从数据库中删除用户：" << usrAccount << "失败";
		return false;
	}

	PQclear(ret);
	OLOG_D() << "从数据库中删除用户：" << usrAccount;
	return true;
}

/**
*	@param	users 返回的用户信息列表，以用户名为key的map容器
*	@return	查询成功返回true，即使成功数据库也可能没有数据
*/
bool N_ORDERSYS::LoginUserDbOperator::lookStaff(std::map<std::string, LoginUser>& users) const
{
	users.clear();
	if (mDBconnect == nullptr)
		return false;
	std::string sql = "SELECT * FROM \"" + mUserTableName + "\"";
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_TUPLES_OK != PQresultStatus(ret)) {
		PQclear(ret);
		return false;
	}
	for (int row = 0; row < PQntuples(ret); ++row) {
		LoginUser user;
		for (int col = 0; col < PQnfields(ret); ++col) {
			if (false == user.fromDBstr(static_cast<PARAM_INDEX>(col), PQgetvalue(ret, row, col))) {
				PQclear(ret);
				OLOG_W() << "从数据库中查询用户时发生错误";
				return false;
			}
		}
		users[user.getAccount()] = (user);
	}
	PQclear(ret);
	OLOG_D() << "从数据库中查询所有用户";
	return true;
}

/**
*	@param	usrAccount 要查询的用户的用户名
*	@param	user 如果用户存在，返回用户对象
*	@return	true表示存在用户
*/
bool N_ORDERSYS::LoginUserDbOperator::lookStaff(const std::string & usrAccount, LoginUser & user) const
{
	std::map<std::string, LoginUser > users;
	this->lookStaff(users);
	if (users.find(usrAccount) == users.end())
		return  false;
	user = users.at(usrAccount);
	return true;
}
