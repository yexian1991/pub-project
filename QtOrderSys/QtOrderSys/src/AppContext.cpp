﻿#include "AppContext.h"
#include "DishList.h"
#include <fstream>


void N_ORDERSYS::AppContext::init()
{
	if (load() == false) {
		mContext.reset();
		exit(-1);
	}
}

void N_ORDERSYS::AppContext::initAfterDb()
{
	//菜单，目前是哪个菜单
	if (false == SupportDishList::instance().init()) {
		OLOG_E() << "INIT SUPPORT DISH LIST FAILED";
	}
	OLOG_D() << "INIT SUPPORT DISH LIST SUCESS";

}

bool N_ORDERSYS::AppContext::save()
{
	if (mContext.IsNull() == true) {
		OLOG_E() << "CANT SAVE CONTEXT FILE ,CONTEXT IS NULL";
		return false;
	}
	std::ofstream ss(mConfigFilePath);
	if (ss.is_open() == false) {
		OLOG_E() << "CANT SAVE CONTEXT FILE ,OPEN FILE FAILED：" << mConfigFilePath;
		return false;
	}
	ss << mContext;
	ss.close();
	OLOG_D() << "SAVE CONTEXT FILE SUCESS：" << mConfigFilePath;
	return true;
}

bool N_ORDERSYS::AppContext::load()
{
	try {
		mContext = YAML::LoadFile(mConfigFilePath);
	}
	catch (...) {
		OLOG_E() << "LOAD CONFIGURE FILE FIELD：" << mConfigFilePath;
		return false;
	}
	OLOG_D() << "LOAD CONFIGURE FILE SUCESS：" << mConfigFilePath;
	return true;
}
