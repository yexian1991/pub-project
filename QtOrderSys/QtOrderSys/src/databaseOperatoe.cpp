﻿#include "databaseOperatoe.h"
#include "log.h"
#include <memory>


using namespace N_ORDERSYS;

N_ORDERSYS::DataBaseOperator::DataBaseOperator()
{
	if (AppContext::instance().query("DBCONNECTSTR", mConnStr) == false) {
		mConnStr = "dbname=ordersys";
	}
}

bool DataBaseOperator::initDataBase()
{
	//检查数据库和数据表是否存在

	return true;
}

std::shared_ptr<PGconn> N_ORDERSYS::DataBaseOperator::getDBConnect()
{
	PGconn *tConn{ nullptr };

	tConn = PQconnectdb(mConnStr.c_str());
	if (PQstatus(tConn) != CONNECTION_OK) {
		PQfinish(tConn);
		return nullptr;
	}
	return  std::shared_ptr<PGconn>(tConn, [](PGconn* conn) {
		PQfinish(conn); 
	});
}
