﻿#include "CookDish.h"
#include "DishList.h"
#include <sstream>

using namespace N_ORDERSYS;

DISHTYPE N_ORDERSYS::CookDish::getCookType(int id)
{
	return static_cast<DISHTYPE>(id / 100);
}

DISHTYPE N_ORDERSYS::CookDish::getCookType(const CookDish & dish)
{
	return static_cast<DISHTYPE>(dish.getId() / 100);
}

CookDish::CookDish()
{
}

N_ORDERSYS::CookDish::CookDish(int id)
{
	auto tSupportLst = SupportDishList::instance().getOrderList();

	for (auto itTuple : tSupportLst) {
		for (auto it : itTuple.second) {
			if (it.getId() == id) {
				*this = it;
				this->setCount();
				return;
			}
		}
	}
}


CookDish::~CookDish()
{
}

N_ORDERSYS::CookDish::CookDish(const CookDish & ths)
{
	*this = ths;
}

CookDish & N_ORDERSYS::CookDish::operator=(const CookDish & ths)
{
	this->mId = ths.mId;
	this->mDishName = ths.mDishName;
	this->mDishPrice = ths.mDishPrice;
	this->mDishType = ths.mDishType;
	this->mDishCount = ths.mDishCount;
	return *this;
}

bool N_ORDERSYS::CookDish::isSameId(const CookDish & ths) const
{
	return this->mId == ths.mId;
}

bool N_ORDERSYS::CookDish::operator==(const CookDish & ths) const
{
	if (this->getId() != ths.getId() || this->getDishName() != ths.getDishName() ||
		this->getPrice() != ths.getPrice() || this->getType() != ths.getType() ||
		this->getCount() != ths.getCount())
		return false;
	return true;
}

bool N_ORDERSYS::CookDish::operator!=(const CookDish & ths) const
{
	return (*this == ths )==false;
}

bool N_ORDERSYS::CookDish::operator>(const CookDish & ths) const
{
	return this->mId > ths.mId;
}

bool N_ORDERSYS::CookDish::operator<(const CookDish & ths) const
{
	return this->mId < ths.mId;
}

std::string N_ORDERSYS::CookDish::toDBstr() const
{


	//INSERT INTO xx VALUES(ss);
	///TODO 要增加一个生成器实现这个过程
	std::ostringstream ss;
	ss << mId << ","
		<< "\'" << mDishName << "\'" << ","
		<< mDishType << ","
		<< mDishPrice << ","
		<< mDishCount;
	return ss.str();
}

bool N_ORDERSYS::CookDish::fromDBstr(int index, const std::string & value)
{
	try
	{
		switch (index)
		{
		case 0:
			mId = stoi( value);
			break;
		case 1:
			mDishName = value.c_str();
			break;
		case 2:
			mDishType = static_cast<DISHTYPE>(stoi(value));
			break;
		case 3:
			mDishPrice = stoi(value);
			break;
		case 4:
			mDishCount = stoi(value);
			break;
		default:
			break;
		}
	}
	catch (const std::exception&)
	{
		return false;
	}
	return true;
}
