#include "LoginUserLmt.h"

std::map<int, std::string > N_ORDERSYS::LoginUserLmt::sLmtMap;

void N_ORDERSYS::LoginUserLmt::initLmtMap()
{
	//后续需要转为可以从文件配置
	sLmtMap[0] = "点菜";
	sLmtMap[1] = "财务";
	sLmtMap[2] = "厨师";
	sLmtMap[3] = "人事";
}

N_ORDERSYS::LoginUserLmt::LoginUserLmt()
{
}


N_ORDERSYS::LoginUserLmt::~LoginUserLmt()
{
}


/**
*	@brief
*
*	@param value 权限值 10进制，转为2进制后每一位表示一个权限
*	比如3->0b0011表示具有1、2项权限
*/
N_ORDERSYS::LoginUserLmt::LoginUserLmt(int value)
{
	if (value <= 0)
		return;
	for (auto pos = 0; pos < mLmt.size(); ++pos) {
		mLmt.set(pos, (value >> pos) % 2);
	}
}

bool N_ORDERSYS::LoginUserLmt::hasLmt(int code)
{
	return mLmt.test(code);
}

void N_ORDERSYS::LoginUserLmt::setLmt(int code, bool has)
{
	mLmt.set(code, has);
}

int N_ORDERSYS::LoginUserLmt::getValue()
{
	return int(mLmt.to_ulong());
}

