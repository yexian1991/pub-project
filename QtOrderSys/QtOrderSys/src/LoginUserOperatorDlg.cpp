﻿#include "LoginUserOperatorDlg.h"
#include "LoginUserAlterDlg.h"
#include "qcombobox.h"
#include "databaseOperatoe.h"
#include "qmessagebox.h"
#include "log.h"
#include"LoginUserDbOperator.h"

using namespace N_ORDERSYS;

///
/// @param users 账户的Map数据，first表示账户名，second是对应的账户对象
/// @return 返回true表示查询成功
bool N_ORDERSYS::LoginUserOperator::lookStaff(std::map<std::string, LoginUser>& users)
{
	return LoginUserDbOperator(N_ORDERSYS::DataBaseOperator::instance().getDBConnect()).lookStaff(users);
}

///
/// @return 返回表头的Map数据，first表示属性key，second是对应名称
///
std::map<decltype(PI_ACCOUNT), std::string > N_ORDERSYS::LoginUserOperator::getItemlabel()
{
	std::map<decltype(PI_ACCOUNT), std::string > lables;
	lables[PI_ACCOUNT] = ("账户");
	lables[PI_NAME] = ("姓名");
	lables[PI_PWD] = ("密码");
	lables[PI_PHONE] = ("电话");
	lables[PI_ADDER] = ("地址");
	lables[PI_AGE] = ("年龄");
	lables[PI_SEX] = ("性别");
	lables[PI_LMT] = ("权限");

	return lables;
}

bool N_ORDERSYS::LoginUserOperator::addStaff(const LoginUser & user)
{
	return LoginUserDbOperator(N_ORDERSYS::DataBaseOperator::instance().getDBConnect()).addStaff(user);
}

bool N_ORDERSYS::LoginUserOperator::removeStaff(const std::string & account)
{
	return LoginUserDbOperator(N_ORDERSYS::DataBaseOperator::instance().getDBConnect()).removeStaff(account);
}

bool N_ORDERSYS::LoginUserOperator::alterStaff(const LoginUser & user)
{
	return LoginUserDbOperator(N_ORDERSYS::DataBaseOperator::instance().getDBConnect()).alterStaff(user);
}

///
/// @param account 要查询的账户
/// @param user 如果存在该账户，以对象引用的方式返回
/// @return 返回true表示查询到指定账户
bool N_ORDERSYS::LoginUserOperator::lookStaff(const std::string & account, LoginUser & user)
{
	return LoginUserDbOperator(N_ORDERSYS::DataBaseOperator::instance().getDBConnect()).lookStaff(account, user);
}




LoginUserOperatorDlg::LoginUserOperatorDlg(QWidget *parent, Qt::WindowFlags f ):QDialog(parent,f)
{
	mOperator = std::make_shared<LoginUserOperator>();

	initFB();
	initTable();
	bindOperator();
}


LoginUserOperatorDlg::~LoginUserOperatorDlg()
{
}

void N_ORDERSYS::LoginUserOperatorDlg::initFB()
{
	this->setWindowTitle(QString::fromLocal8Bit("人员账号管理"));
	this->setObjectName(QString::fromUtf8("LoginUserOperatorDlg"));
	this->resize(800, 600);

	auto	horizontalLayout_2 = new QHBoxLayout(this);
	horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
	auto horizontalLayout = new QHBoxLayout();
	horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));

	mTableWidget = new QTableWidget(this);

	//表头填充
	auto itemLabels=mOperator->getItemlabel();
	mTableWidget->setColumnCount(itemLabels.size());
	for (const auto & item: itemLabels) {
		mTableWidget->setHorizontalHeaderItem(item.first, new QTableWidgetItem(QString::fromLocal8Bit(item.second.c_str())));
	}
	//mTableWidget->setHorizontalHeaderItem(PI_ACCOUNT, new QTableWidgetItem(QString::fromLocal8Bit("账号")));
	
	mTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
	mTableWidget->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);

	mTableWidget->setObjectName(QString::fromUtf8("mTableWidget"));
	horizontalLayout->addWidget(mTableWidget);

	auto verticalLayout = new QVBoxLayout();
#define ADDPUSHBTN(PARAM,DISPLAY)\
	PARAM = new QPushButton(QString::fromLocal8Bit(#DISPLAY),this);\
	verticalLayout->addWidget(PARAM);\

	ADDPUSHBTN(mBtnReflush, 刷新);
	ADDPUSHBTN(mBtnCheck, 查看);
	ADDPUSHBTN(mBtnAdd, 增加);
	ADDPUSHBTN(mBtnAlter, 修改);
	ADDPUSHBTN(mBtnRemove, 删除);
	ADDPUSHBTN(mBtnOk, 确定);
	ADDPUSHBTN(mBtnCancel, 退出);

	auto verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
	verticalLayout->addItem(verticalSpacer);
#undef ADDPUSHBTN

	horizontalLayout->addLayout(verticalLayout);
	horizontalLayout_2->addLayout(horizontalLayout);
}

void N_ORDERSYS::LoginUserOperatorDlg::initTable() {
	mTableWidget->clearContents();
	//读战鼓列表
	std::map<std::string, LoginUser > users;
	if (false == mOperator->lookStaff(users)) {
		OLOG_W() << "无法获取用户列表";
		return;
	}
	//填充表格
	mTableWidget->setRowCount(users.size());

	int row = 0;
	for (const auto& item: users) {
		auto user = item.second;
		mTableWidget->setItem(row, PI_ACCOUNT, new QTableWidgetItem(QString(user.getAccount().c_str())));
		mTableWidget->setItem(row, PI_NAME, new QTableWidgetItem(QString(user.getName().c_str())));
		mTableWidget->setItem(row, PI_PWD, new QTableWidgetItem(QString(user.getPassword().c_str())));
		mTableWidget->setItem(row, PI_PHONE, new QTableWidgetItem(QString(user.getPhone().c_str())));
		mTableWidget->setItem(row, PI_ADDER, new QTableWidgetItem(QString(user.getAdder().c_str())));
		mTableWidget->setItem(row, PI_AGE, new QTableWidgetItem(QString::number(user.getAge())));

		QComboBox *tSexCombox = new QComboBox(); // 下拉选择框控件
		tSexCombox->addItem(QString::fromLocal8Bit("女"));
		tSexCombox->addItem(QString::fromLocal8Bit("男"));
		tSexCombox->setCurrentIndex(user.isFemale() ? 0 : 1);
		mTableWidget->setCellWidget(row, PI_SEX, (QWidget*)tSexCombox);

		mTableWidget->setItem(row, PI_LMT, new QTableWidgetItem(QString::number(user.getLmt())));//要改进成具体的内容

		++row;
	}

}

void N_ORDERSYS::LoginUserOperatorDlg::bindOperator()
{

	connect(mBtnCheck, &QPushButton::clicked, [=]() {
		if (isSelectRow() == false)
			return;
		auto account = mTableWidget->item(mTableWidget->currentRow(), PI_ACCOUNT)->text().toStdString();
		
		LoginUser user;
		if (mOperator->lookStaff(account, user) == false)
			return;
		OLOG_D() << "查看账户信息：" << account;
		LoginUserAlterDlgCheck dlg{ user };
		dlg.exec();
	});

	connect(mBtnAdd, &QPushButton::clicked, [=]() {
		LoginUserAlterDlgAdd dlg;
		if (dlg.exec() == QDialog::Rejected) {
			OLOG_D() << "添加账户取消";
			return;
		}

		LoginUser usr{ dlg.getValue() }; 
		if (mOperator->addStaff(usr) == true) {
			QMessageBox::about(this, "tips", QString::fromLocal8Bit("添加用户成功！"));
			OLOG_D() << "添加用户成功";
			initTable();
		}
		else{
			QMessageBox::warning(this, "tips", QString::fromLocal8Bit("添加用户失败！"));
			OLOG_E() << "添加用户失败";
		}
	});

	connect(mBtnRemove, &QPushButton::clicked, [=]() {
		if (isSelectRow() == false)
			return;
		auto account = mTableWidget->item(mTableWidget->currentRow(), PI_ACCOUNT)->text().toStdString();
		
		if (QMessageBox::Cancel == QMessageBox::question(this, "tips", QString::fromLocal8Bit("确定删除用户吗？"), 
			QMessageBox::Ok | QMessageBox::Cancel)) 
		{
			return;
		}
		
		if (mOperator->removeStaff(account) == true) {
			QMessageBox::about(this, "tips", QString::fromLocal8Bit("删除用户成功！"));
			OLOG_D() << "删除用户成功";
			initTable();
		}
		else {
			QMessageBox::warning(this, "tips", QString::fromLocal8Bit("删除用户失败！"));
			OLOG_E() << "删除用户失败";
		}
	});

	connect(mBtnAlter, &QPushButton::clicked, [=]() {
		if (isSelectRow() == false)
			return;
		auto account = mTableWidget->item(mTableWidget->currentRow(), PI_ACCOUNT)->text().toStdString();
		LoginUser user;
		if (mOperator->lookStaff(account, user) == false)
			return;

		LoginUserAlterDlgAlter dlg{ user };
		if (dlg.exec() == QDialog::Rejected)
			return;
		LoginUser usr{ dlg.getValue() };
		if (mOperator->alterStaff(usr) == true) {
			QMessageBox::about(this, "tips", QString::fromLocal8Bit("修改用户成功！"));
			OLOG_D() << "修改用户成功";
			initTable();
		}
		else {
			QMessageBox::warning(this, "tips", QString::fromLocal8Bit("修改用户失败！"));
			OLOG_E() << "修改用户失败";
		}
	});

	connect(mBtnReflush, &QPushButton::clicked, [=]() {initTable();	});
	connect(mBtnOk, &QPushButton::clicked, this, &QDialog::accept);
	connect(mBtnCancel, &QPushButton::clicked, this, &QDialog::reject);
}

bool N_ORDERSYS::LoginUserOperatorDlg::isSelectRow()
{
	if (mTableWidget == nullptr)
		return false;
	if (0 == mTableWidget->selectedItems().size())
		return false;
	return true;
}
