﻿#include "LoginUser.h"
#include <sstream>
using namespace N_ORDERSYS;


LoginUser::LoginUser()
{
}

N_ORDERSYS::LoginUser::LoginUser(const LoginUser & e)
{
	mName = e.getName();
	mAccount = e.getAccount();
	mPassword = e.getPassword();
	mSex = e.isFemale() ? 0 : 1;
	mAge = e.getAge();
	mPhone = e.getPhone();
	mAdder = e.getAdder();
	mLmt = e.getLmt();
}

N_ORDERSYS::LoginUser::LoginUser(const std::string & account, const std::string & name,
	const std::string & pwd, const std::string & phone, int age, int sex, const std::string & addr, int lmt)
{
	mName = name;
	mAccount = account;
	mPassword = pwd;
	mSex = sex;
	mAge = age;
	mPhone = phone;
	mAdder = addr;
	mLmt = lmt;
}


LoginUser::~LoginUser()
{
}

LoginUser & N_ORDERSYS::LoginUser::operator=(const LoginUser & t)
{
	mName = t.getName();
	mAccount = t.getAccount();
	mPassword = t.getPassword();
	mSex = t.isFemale() ? 0 : 1;
	mAge = t.getAge();
	mPhone = t.getPhone();
	mAdder = t.getAdder();
	mLmt = t.getLmt();
	return *this;
}


/**
*	@return first表示为false表示不满足要求，second为不满足要求的提示
*/
std::pair<bool, std::string> N_ORDERSYS::LoginUser::isPerfect() const
{
	if (mAccount.size() <= 0 || mAccount.size() > 20)
		return std::make_pair<>(false, std::string("ACCOUNT IS ERROR"));
	if (mPassword.size() <= 0 || mPassword.size() > 20)
		return std::make_pair<>(false, std::string("PWD IS ERROR"));

	//...
	return std::make_pair<>(true, std::string(""));
}

bool N_ORDERSYS::LoginUser::operator==(const LoginUser & usr)
{
#define EQUALXX(KEY)\
	if (this->KEY != usr.KEY)\
		return false;
	EQUALXX(mAccount);
	EQUALXX(mName);
	EQUALXX(mPassword);
	EQUALXX(mSex);
	EQUALXX(mAge);
	EQUALXX(mPhone);
	EQUALXX(mAdder);
	EQUALXX(mLmt);
#undef EQUALXX

	return true;
}

/**
*	@return 返回数据库中使用的字符串，格式为'val1','val2',...'valn'
*/
std::string N_ORDERSYS::LoginUser::toDBstr() const
{
	std::ostringstream ss;
	ss << "\'" << mAccount << "\'" << ","
		<< "\'" << mName << "\'" << ","
		<< "\'" << mPassword << "\'" << ","
		<< "\'" << mPhone << "\'" << ","
		<< "\'" << mAdder << "\'" << ","
		<< mAge << ","
		<< mSex << ","
		<< mLmt;
	return ss.str();
}


/**
*	@param	index 一项内容，必须小于PARAM_INDEX::PL_MAX的值
*	@param	value 此项对应的字符串值，转换后保存到本对象
*	@return	正确转换返回true
*/
bool N_ORDERSYS::LoginUser::fromDBstr(decltype(PARAM_INDEX::PL_MAX)  index, const std::string& value)
{
	try
	{
		switch (index)
		{
		case N_ORDERSYS::PI_ACCOUNT:
			mAccount = value;
			break;
		case N_ORDERSYS::PI_NAME:
			mName = value;
			break;
		case N_ORDERSYS::PI_PWD:
			mPassword = value;
			break;
		case N_ORDERSYS::PI_PHONE:
			mPhone = value;
			break;
		case N_ORDERSYS::PI_ADDER:
			mAdder = value;
			break;
		case N_ORDERSYS::PI_AGE:
			mAge = stoi(std::string(value));
			break;
		case N_ORDERSYS::PI_SEX:
			mSex = stoi(value);
			break;
		case N_ORDERSYS::PI_LMT:
			mLmt = stoi(value);
			break;
		default:
			break;
		}
	}
	catch (const std::exception&)
	{
		return false;
	}
	return true;
}


