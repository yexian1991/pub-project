#include "QtOrderSys.h"
#include "LoginUserOperatorDlg.h"
#include "TableListWidget.h"
#include "qlabel.h"
#include "qtimer.h"
using namespace N_ORDERSYS;

QtOrderSys::QtOrderSys(QWidget *parent)
    : QMainWindow(parent)
{
	ui.setupUi(this);
	initFB();
	initBind();
}

void QtOrderSys::initFB() {
	initAction();
	initMenu();
	initToolBar();
	initStatusBar();
	
	mCenterWidget = new QWidget(this);
	mCenterWidget->setStyleSheet("border-image:url(:/QtOrderSys/res/homepage.jpeg)");
	setCentralWidget(mCenterWidget);
	this->setWindowState(Qt::WindowMaximized);
}

void QtOrderSys::initAction(){
	mAcTable							= new QAction(QIcon(":/QtOrderSys/res/icon/iconTable.png"), QString::fromLocal8Bit("开台列表"), this);
	//mAcOpenTable					= new QAction(QIcon(":/QtOrderSys/res/icon/iconOpenTable.png"), QString::fromLocal8Bit("台面"), this);
	//mAcDinner							= new QAction(QIcon(":/QtOrderSys/res/icon/iconDinner.png"), QString::fromLocal8Bit("点菜"), this);
	//mAcOperator						= new QAction(QIcon(":/QtOrderSys/res/icon/iconOperator.png"), QString::fromLocal8Bit("加减菜"), this);
	//mAcSettle								= new QAction(QIcon(":/QtOrderSys/res/icon/iconSettle.png"), QString::fromLocal8Bit("结账"), this);

	mAcAddFoodStyle				= new QAction(QString::fromLocal8Bit("增加菜式"), this);
	mAcRemoveFoodStyle			= new QAction(QString::fromLocal8Bit("删除菜式"), this);
	mAcAlterFoodStyle				= new QAction(QIcon(":/QtOrderSys/res/icon/iconAlterFoodStyle.png"), QString::fromLocal8Bit("修改菜式"), this);
	mAcAlterFoodCount			= new QAction(QIcon(":/QtOrderSys/res/icon/iconAlterFoodCount.png"), QString::fromLocal8Bit("修改库存"), this);

	mAcDailyBill						= new QAction(QIcon(":/QtOrderSys/res/icon/iconBill.png"), QString::fromLocal8Bit("日账"), this);
	mAcMonthlyBill					= new QAction(QString::fromLocal8Bit("年账"), this);
	mAcAnnualBill						= new QAction(QString::fromLocal8Bit("月账"), this);

	mAcLookStaff						= new QAction(QIcon(":/QtOrderSys/res/icon/iconAlterStaff.png"), QString::fromLocal8Bit("查阅员工"), this);
	mAcAddStaff						= new QAction(QString::fromLocal8Bit("增加员工"), this);
	mAcRemoveStaff					= new QAction(QString::fromLocal8Bit("删除员工"), this);
	mAcAlterStaff						= new QAction(QString::fromLocal8Bit("修改员工"), this);

	mAcHelp								= new QAction(QString::fromLocal8Bit("帮助"), this);
	mAcAbout							= new QAction(QIcon(":/QtOrderSys/res/icon/iconAbout.png"), QString::fromLocal8Bit("关于"), this);
}

void QtOrderSys::initMenu(){
	menuBar = new QMenuBar(this);
	QMenu* tableMenu = menuBar->addMenu(QString::fromLocal8Bit("点餐"));
	QMenu* backMenu = menuBar->addMenu(QString::fromLocal8Bit("后台"));
	QMenu* financeMenu = menuBar->addMenu(QString::fromLocal8Bit("财务"));
	QMenu* staffMenu = menuBar->addMenu(QString::fromLocal8Bit("人员"));
	QMenu* aboutMenu = menuBar->addMenu(QString::fromLocal8Bit("关于"));
	setMenuBar(menuBar);

	tableMenu->addAction(mAcTable);
	//tableMenu->addAction(mAcOpenTable);
	//tableMenu->addAction(mAcDinner);
	//tableMenu->addAction(mAcOperator);
	//tableMenu->addAction(mAcSettle);

	backMenu->addAction(mAcAddFoodStyle);
	backMenu->addAction(mAcRemoveFoodStyle);
	backMenu->addAction(mAcAlterFoodStyle);
	backMenu->addAction(mAcAlterFoodCount);

	financeMenu->addAction(mAcDailyBill);
	financeMenu->addAction(mAcMonthlyBill);
	financeMenu->addAction(mAcAnnualBill); 

	staffMenu->addAction(mAcLookStaff);
	staffMenu->addAction(mAcAddStaff);
	staffMenu->addAction(mAcRemoveStaff);
	staffMenu->addAction(mAcAlterStaff);
	aboutMenu->addAction(mAcHelp);
	aboutMenu->addAction(mAcAbout);
}

void QtOrderSys::initToolBar(){
	QToolBar *toolBar = new QToolBar(this);
	toolBar->setIconSize(QSize(80, 80));
	addToolBar(toolBar);
	toolBar->addAction(mAcTable);
	//toolBar->addAction(mAcOpenTable);
	//toolBar->addAction(mAcDinner);
	//toolBar->addAction(mAcOperator);
	//toolBar->addAction(mAcSettle);
	toolBar->addSeparator();

	toolBar->addAction(mAcAlterFoodStyle);
	toolBar->addAction(mAcAlterFoodCount);
	toolBar->addSeparator();

	toolBar->addAction(mAcDailyBill);
	toolBar->addSeparator();

	toolBar->addAction(mAcLookStaff);
	toolBar->addSeparator();
	toolBar->addAction(mAcAbout);

	toolBar->setAllowedAreas(Qt::LeftToolBarArea);//可停靠区域
	toolBar->setMovable(false);//是否可移动
	toolBar->setFloatable(false);//是否浮动

}

void QtOrderSys::initStatusBar(){
	//添加状态栏
	QStatusBar *status = new QStatusBar();
	setStatusBar(status);
	//标签放入状态栏
	QLabel *timeLabel = new QLabel("time");
	status->addWidget(timeLabel);

	QLabel *label2 = new QLabel("Right");
	status->addPermanentWidget(label2);//右对齐
	//动态修改状态栏的内容显示

	QTimer *timer = new QTimer();
	connect(timer, &QTimer::timeout, [=]() {
		//ctime回车问题的解决
		auto today = std::chrono::system_clock::now();
		auto todaytt = std::chrono::system_clock::to_time_t(today);
		auto todayStr=ctime(&todaytt);
		todayStr[strlen(todayStr) - 1] = 0;
		timeLabel->setText(todayStr);
	});
	timer->start(1000);
}

void QtOrderSys::initBind() {
	connect(mAcLookStaff, &QAction::triggered, [=]() {LoginUserOperatorDlg dlg; dlg.exec(); });
	connect(mAcTable, &QAction::triggered, [=]() {
		mCenterWidget = new TableListWidget(this);
		setCentralWidget(mCenterWidget);
	});
}