#include "log.h"
#include <stdarg.h>
//#include <varargs.h>
#include <iostream>
#include <chrono>
#include <sstream>

//格式化消息字符串
#define FORMATMSG(lvl,fmt)\
	mLogEvent->setLevel(lvl);\
	va_list var_arg;\
	int len = 0;\
	char buf[1024] = { 0 };\
	va_start(var_arg, fmt);\
	len = std::vsnprintf(buf, 1024, fmt, var_arg);\
	va_end(var_arg);\
	mLogEvent->setMessage(std::string(buf));

//流消息
#define OFORMATMSG(lvl)\
	mLogEvent->setLevel(lvl);\
	return mLogEvent->getSS();

#pragma region ClassLogLevel
std::string Nwendy::LogLevel::toString(LogLevel::Level level)
{
	switch (level) {
#define XX(name) \
    case LogLevel::name: \
        return #name; \
        break;

		XX(LL_DEBUG);
		XX(LL_INFO);
		XX(LL_WARN);
		XX(LL_ERROR);
		XX(LL_FATAL);
#undef XX
	default:
		return "LL_NONE";
	}
	return "LL_NONE";
}

Nwendy::LogLevel::Level Nwendy::LogLevel::fromString(const std::string& str)
{
#define XX(level, v) \
    if(str == #v) { \
        return LogLevel::level; \
    }

	XX(LL_DEBUG, LL_DEBUG);
	XX(LL_INFO, LL_INFO);
	XX(LL_WARN, LL_WARN);
	XX(LL_ERROR, LL_ERROR);
	XX(LL_FATAL, LL_FATAL);
	return LogLevel::LL_NONE;
#undef XX
}

#pragma endregion ClassLogLevel

#pragma region ClassLogEvent
Nwendy::LogEvent::LogEvent(const std::string& fileName, const std::string& functionName, int line)
	:mFileName(fileName), mFunctionName(functionName), mLineNo(line)
{
	mThreadId = std::this_thread::get_id();
	mTime = time(0);
}

std::string Nwendy::LogEvent::toString(LogEvent::ptr ptr)
{
	std::stringstream ss;

	if (mParamFlags & static_cast<int>(Nwendy::MessageParams::MP_LEVEL))
		ss << "[" << Nwendy::LogLevel::toString(ptr->mLevel) << "]";
	if (mParamFlags & static_cast<int>(Nwendy::MessageParams::MP_TIME))
		ss << "[" << Nwendy::getTimeStr(ptr->mTime) << "]";
	if (mParamFlags & static_cast<int>(Nwendy::MessageParams::MP_FILENAME))
		ss << "[" << ptr->mFileName << "]";
	if (mParamFlags & static_cast<int>(Nwendy::MessageParams::MP_FUNCNAME))
		ss << "[" << ptr->mFunctionName << "]";
	if (mParamFlags & static_cast<int>(Nwendy::MessageParams::MP_LINE))
		ss << "[Line:" << ptr->mLineNo << "]";
	if (mParamFlags & static_cast<int>(Nwendy::MessageParams::MP_THREADID))
		ss << "[ThreadId:" << ptr->mThreadId << "]";
	if (mParamFlags & static_cast<int>(Nwendy::MessageParams::MP_MESSAGE)) {
		if (mSS.str().size() != 0)
			ptr->mMessage = mSS.str();
		ss << "[" << ptr->mMessage << "]";
	}

	ss << std::endl;
	return ss.str();
}

std::string Nwendy::getTimeStr(time_t t, const std::string& fmt)
{
struct	tm tm1;
#ifdef _WIN32
	localtime_s(&tm1, &t);
#else
	localtime_r(&t, &tm1);
#endif


	char buf[64];
	strftime(buf, sizeof(buf), fmt.c_str(), &tm1);
	return std::string(buf);
}

#pragma endregion ClassLogEvent

#pragma region ClassLogger
Nwendy::Logger::Logger(const char* fileName, const char* functionName, int line)
{
	mLogEvent= std::make_shared<Nwendy::LogEvent>(fileName, functionName, line);
}

void Nwendy::Logger::log(const LogEvent::ptr & event)
{
	if (event->getLevel() >= mMinLogLevel) {
		//event存入缓存中
		LogManager::getInstance().getBuffer()->addLogEvent(event);
	}
}

std::stringstream& Nwendy::Logger::odebug(){OFORMATMSG(Nwendy::LogLevel::LL_DEBUG) }
std::stringstream& Nwendy::Logger::oinfo() { OFORMATMSG(Nwendy::LogLevel::LL_INFO) }
std::stringstream& Nwendy::Logger::owarning() { OFORMATMSG(Nwendy::LogLevel::LL_WARN) }
std::stringstream& Nwendy::Logger::oerror() { OFORMATMSG(Nwendy::LogLevel::LL_ERROR) }
std::stringstream& Nwendy::Logger::ofatal() { OFORMATMSG(Nwendy::LogLevel::LL_FATAL) }

void Nwendy::Logger::debug(const char* fmt, ...) { FORMATMSG(Nwendy::LogLevel::LL_DEBUG, fmt) }
void Nwendy::Logger::info(const char* fmt, ...) { FORMATMSG(Nwendy::LogLevel::LL_INFO, fmt) }
void Nwendy::Logger::warning(const char* fmt, ...) { FORMATMSG(Nwendy::LogLevel::LL_WARN, fmt) }
void Nwendy::Logger::error(const char* fmt, ...) { FORMATMSG(Nwendy::LogLevel::LL_ERROR, fmt) }
void Nwendy::Logger::fatal(const char* fmt, ...) { FORMATMSG(Nwendy::LogLevel::LL_FATAL, fmt) }

Nwendy::LogLevel::Level Nwendy::Logger::mMinLogLevel;

#pragma endregion ClassLogger

#pragma region ClassLogBufferManager
Nwendy::LogBufferManager::LogBufferManager(size_t bufferSize)
{
	mMaxBufferSize = bufferSize;
	mBuffer_1.reserve(bufferSize);
	mBuffer_2.reserve(bufferSize);
}

Nwendy::LogBufferManager::~LogBufferManager()
{
	swapBuffer();
	log2Append();
}

bool Nwendy::LogBufferManager::addLogEvent(const LogEvent::ptr& event)
{
	std::unique_lock<std::mutex> lock(mPushLogMutex);
	mBuffer_1.emplace_back(event);
	if (mBuffer_1.size() >= mMaxBufferSize) {
		swapBuffer();
	}
	return true;
}

void Nwendy::LogBufferManager::setBufferSize(size_t size)
{
	if (mMaxBufferSize < size) {
		{
			std::unique_lock<std::mutex> lock(mBufferMutex);
			mBuffer_2.reserve(size); 
		}
		{
			std::unique_lock<std::mutex> lock(mPushLogMutex);
			mBuffer_1.reserve(size); 
		}
	}
	mMaxBufferSize = size;
}

void Nwendy::LogBufferManager::setTimerSwapInterval(int interval_s)
{
	mTimerSwapInterval_s.store(interval_s);
}

void Nwendy::LogBufferManager::addAppend(LogAppend::ptr ptr)
{
	if (ptr == nullptr)
		return;
	std::unique_lock<std::mutex> lock(mLogAppendMutex);
	mLogAppends.push_back(ptr);
}

void Nwendy::LogBufferManager::removeAppend(LogAppend::ptr ptr)
{
	if (ptr == nullptr)
		return;
	for (auto it = mLogAppends.begin();it != mLogAppends.end();it++) {
		if (ptr == *it) {
			std::unique_lock<std::mutex> lock(mLogAppendMutex);
			mLogAppends.erase(it);
			return;
		}
	}
}

void Nwendy::LogBufferManager::swapBuffer()
{
	std::unique_lock<std::mutex> lock(mBufferMutex);
	mBuffer_1.swap(mBuffer_2);
	mConditionVariable.notify_all();
}

void Nwendy::LogBufferManager::timerSwap()
{
	while (mStop.load() == false) {
		std::this_thread::sleep_for(std::chrono::seconds(mTimerSwapInterval_s.load()));
		swapBuffer();
	}
}

void Nwendy::LogBufferManager::log2Append()
{
	while (mStop.load()==false) {
		std::unique_lock<std::mutex> lock(mBufferMutex);
		mConditionVariable.wait(lock, [&]() {
			return mBuffer_2.size() != 0 || mStop.load() == true;
		});
		{
			std::unique_lock<std::mutex> lock(mLogAppendMutex);
			for (auto &&it : mLogAppends) {
				it->display(mBuffer_2);
			}
		}
		mBuffer_2.clear();
	}
	std::cout << "stop thread==========================" << std::endl;
}

#pragma endregion ClassLogBufferManager

#pragma region ClassLogAppend
void Nwendy::LogStdAppend::display(const std::vector<LogEvent::ptr>& vct)
{
	for (auto &&event : vct) {
		std::cout << event->toString(event);
	}
}

Nwendy::LogFileAppend::LogFileAppend(const std::string & fileDir)
	:mFileDir(fileDir)
{
	mCurFilePath = mFileDir;
	mCurFilePath = mCurFilePath+"/"+ getFileName();
}

void Nwendy::LogFileAppend::display(const std::vector<LogEvent::ptr>& vct)
{
	try {
		auto fs = open();
		for (auto &&event : vct) {
			fs << event->toString(event);
		}
		fs.close();
	}
	catch (const std::exception&) {

	}
}

std::fstream Nwendy::LogFileAppend::open()
{
	std::fstream fs(mCurFilePath, std::ios::app);
	if (fs.is_open() == false) {
		std::logic_error e("cant open log file");
		throw (std::exception(e));
	}
	return fs;
}

std::string Nwendy::LogFileAppend::getFileName()
{
	auto sysclk = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t(sysclk);
	tm ptm;
#ifdef _WIN32
	localtime_s(&ptm, &tt);
#else
	localtime_r(&tt, &ptm);
#endif
	std::stringstream ss;
	ss << "wendy" << 1900 + ptm.tm_year << ptm.tm_mon << ptm.tm_mday<< ".log";
	return ss.str();
}

#pragma endregion ClassLogAppend

#pragma region ClassLogManager
Nwendy::LogManager& Nwendy::LogManager::getInstance()
{
	static LogManager manager;
	return manager;
}

void Nwendy::LogManager::init()
{
	mLogBufferMag = std::make_shared<Nwendy::LogBufferManager>(100);
	
	Logger::setMinLevel(Nwendy::LogLevel::LL_DEBUG);

	mSaveBufferThread = std::move(std::thread(&LogBufferManager::log2Append, mLogBufferMag));
	mTimerSwapThread = std::move(std::thread(&LogBufferManager::timerSwap, mLogBufferMag));
}

void Nwendy::LogManager::setLevel(LogLevel::Level lvl)
{
	Nwendy::Logger::setMinLevel(lvl);
}

void Nwendy::LogManager::setDisplayFmt(int flags/*=static_cast<int>(Nwendy::MessageParams::MP_ALL)*/)
{
	Nwendy::mParamFlags = flags;
}

void Nwendy::LogManager::setBufferSize(int size)
{
	mLogBufferMag->setBufferSize(size);
}

void Nwendy::LogManager::setTimerSwapInterval(int interval_s)
{
	mLogBufferMag->setTimerSwapInterval(interval_s);
}

Nwendy::LogManager::~LogManager()
{
	mLogBufferMag->stopLogSaveThread();
	if (mSaveBufferThread.joinable())
		mSaveBufferThread.join();
	if (mTimerSwapThread.joinable())
		mTimerSwapThread.join();
}

#pragma endregion ClassLogManager
