﻿#include "CookDishDbOperator.h"

N_ORDERSYS::CookDishDbOperator::CookDishDbOperator(const std::shared_ptr<PGconn>& conn)
{
	mDBconnect = conn;
	AppContext::instance().query("DISHTABLENAME", mDishTableName);
}

bool N_ORDERSYS::CookDishDbOperator::hasDish(int id)
{
	if (mDBconnect == nullptr) {
		OLOG_E() << "数据库连接失败，可能没有数据库或者没有初始化数据库的调用";
		return false;
	}

	std::string sql = "SELECT * FROM \"" + mDishTableName + "\" WHERE dishid = " + std::to_string(id);
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_TUPLES_OK != PQresultStatus(ret)) {
		PQclear(ret);
		OLOG_W() << "从数据库查询用户时失败";
		return false;
	}
	if (PQntuples(ret) == 0) {
		PQclear(ret);
		OLOG_W() << "数据库中没有菜式：" << id;
		return false;
	}
	PQclear(ret);
	OLOG_D() << "数据库中查询到菜式：" << id;
	return true;
}

bool N_ORDERSYS::CookDishDbOperator::addDish(const CookDish & dish)
{
	//INSERT INTO "tAccount" VALUES('admin','张三','Password123','18273111773','',31,1,0);
	if (mDBconnect == nullptr)
		return false;
	std::string sql = std::string("INSERT INTO ") + "\"" + mDishTableName + "\"" + " VALUES " + "(" + dish.toDBstr() + ");";
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	auto code = PQresultStatus(ret);
	if (PGRES_COMMAND_OK != code) {
		PQclear(ret);
		OLOG_W() << "向数据库中添加菜式：" << dish.getDishName() << "失败";
		return false;
	}

	PQclear(ret);
	OLOG_D() << "向数据库中添加用户：" << dish.getDishName();
	return true;
}

bool N_ORDERSYS::CookDishDbOperator::removeDish(int id)
{
	if (hasDish(id) == false)
		return false;

	//DELETE FROM "tAccount" WHERE "account"='admin';
	std::string sql = std::string("DELETE FROM ") + "\"" + mDishTableName + "\"" + " WHERE " + "dishid=" + std::to_string(id) ;
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_COMMAND_OK != PQresultStatus(ret)) {
		PQclear(ret);
		OLOG_W() << "从数据库中删除用户：" << id << "失败";
		return false;
	}

	PQclear(ret);
	OLOG_D() << "从数据库中删除用户：" << id;
	return true;
}

bool N_ORDERSYS::CookDishDbOperator::alterDish(const CookDish & dish)
{
	return false;
}

bool N_ORDERSYS::CookDishDbOperator::alterXDishCount(int id, int count)
{
	if (hasDish(id) == false)
		return false;

	std::string sql = std::string("UPDATE ") + "\"" + mDishTableName + "\"" + " SET " + "dishcount=" +std::to_string(count)+" WHERE dishid=" + std::to_string(id);
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	auto code = PQresultStatus(ret);
	if (PGRES_COMMAND_OK != code) {
		PQclear(ret);
		OLOG_W() << "向数据库中减少菜式库存失败";
		return false;
	}
	return false;
}

bool N_ORDERSYS::CookDishDbOperator::lookXDishCount(int id, int & count) 
{
	if (hasDish(id) == false)
		return false;
	std::string sql = "SELECT dishcount FROM \"" + mDishTableName + "\" WHERE dishid = " + std::to_string(id) ;
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_TUPLES_OK != PQresultStatus(ret)) {
		PQclear(ret);
		return false;
	}
	count = atoi(PQgetvalue(ret, 0, 0));
	PQclear(ret);
	return true;
}

bool N_ORDERSYS::CookDishDbOperator::lookXDishPrice(int id, int & count)
{
	if (hasDish(id) == false)
		return false;
	std::string sql = "SELECT dishprice FROM \"" + mDishTableName + "\" WHERE dishid = " + std::to_string(id);
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_TUPLES_OK != PQresultStatus(ret)) {
		PQclear(ret);
		return false;
	}
	count = atoi(PQgetvalue(ret, 0, 0));
	PQclear(ret);
	return true;
}

bool N_ORDERSYS::CookDishDbOperator::lookStaff(CookListMap& dishs)
{
	dishs.clear();
	if (mDBconnect == nullptr)
		return false;
	std::string sql = "SELECT * FROM \"" + mDishTableName + "\"";
	PGresult * ret = PQexec(mDBconnect.get(), sql.c_str());
	if (PGRES_TUPLES_OK != PQresultStatus(ret)) {
		PQclear(ret);
		return false;
	}
	for (int row = 0; row < PQntuples(ret); ++row) {
		CookDish dish;
		for (int col = 0; col < PQnfields(ret); ++col) {
			if (false == dish.fromDBstr(col, PQgetvalue(ret, row, col))) {
				PQclear(ret);
				OLOG_W() << "从数据库中查询菜式时发生错误";
				return false;
			}
		}
		dishs[dish.getType()].emplace_back(dish);
	}
	PQclear(ret);
	OLOG_D() << "从数据库中查询所有菜式";
	return true;
}

bool N_ORDERSYS::CookDishDbOperator::lookStaff(int id, CookDish & dish)
{
	CookListMap dishs;
	this->lookStaff(dishs);
	for (auto &itVct : dishs) {
		for (auto & it : itVct.second){
			if (it.getId() == id) {
				dish = it;
				return true;
			}
		}
	}
	return  false;
}
