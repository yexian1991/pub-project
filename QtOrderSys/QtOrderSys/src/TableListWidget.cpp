#include "TableListWidget.h"
#include "TableServerDlg.h"
using namespace N_ORDERSYS;


N_ORDERSYS::TableListWidget::TableListWidget(QWidget * parent, Qt::WindowFlags f)
	:QWidget(parent,f)
{
	mTableList = new	QListWidget(this);
	mTableList->setFlow(QListView::LeftToRight);
	mTableList->setResizeMode(QListView::Adjust);
	mTableList->setGridSize(QSize(300, 300));
	mTableList->setIconSize(QSize(160, 200));
	mTableList->setItemAlignment(Qt::AlignCenter);
	mTableList->setMovement(QListView::Movement::Static);
	mTableList->setViewMode(QListView::IconMode);
	mTableList->setUniformItemSizes(true);
	
	for (int index = 0; index < 100; index++) {
		auto item = new QListWidgetItem(QIcon(":/QtOrderSys/res/icon/iconOpenTable.png"), QString::number(index));
		item->setTextAlignment(Qt::AlignCenter);
		auto ft = item->font();ft.setPointSize(30);
		item->setFont(ft);

		mTableManagers.emplace_back(std::make_shared<TableManager>(index, TableManager::TableMounment::TM_SMALL));

		mTableList->addItem(item);
	}


	connect(mTableList, &QListWidget::itemDoubleClicked, [=](QListWidgetItem *item) {
		auto id = item->text().toInt();

		TableServerDlg::ptr  Ptr= std::make_shared<TableServerDlg>(mTableManagers[id]);
		Ptr->exec();

		auto color = N_ORDERSYS::TableStateMap::instance().getTableColor(mTableManagers[id]->getStateNow());
		item->setBackgroundColor(QColor(color));
	});

}

TableListWidget::~TableListWidget()
{

}

void N_ORDERSYS::TableListWidget::resizeEvent(QResizeEvent * event)
{
	mTableList->resize(this->size());

	QWidget::resizeEvent(event);
}
