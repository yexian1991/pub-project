#include "OrderDlg.h"
#include "qtreewidget.h"
#include "qboxlayout.h"
#include "databaseOperatoe.h"
#include "CookDishDbOperator.h"
#include "qmessagebox.h"




using namespace N_ORDERSYS;


OrderDlg::OrderDlg(DishList::ptr lst, DishList::ptr rst)
	:mLeftList(*lst), mRightList(*rst)
{
	create();
}

N_ORDERSYS::OrderDlg::OrderDlg(const DishList & lst, const DishList & rst)
	:mLeftList(lst), mRightList(rst)
{
	create();
}


OrderDlg::~OrderDlg()
{
}

void N_ORDERSYS::OrderDlg::create()
{
	initFB();
	initValue();

	connect(mBtn2Right, &QPushButton::clicked, this, &OrderDlg::onAdd2Right);
	connect(mBtn2Left, &QPushButton::clicked, this, &OrderDlg::onRemoveFromRight);
	connect(pushApply, &QPushButton::clicked, this, &OrderDlg::onApplyOrder);
	connect(mBtnOk, &QPushButton::clicked, this, &OrderDlg::onOkOrder);
	connect(pushCancel, &QPushButton::clicked, this, &QDialog::reject);
	connect(mTreeLeft, &QTreeWidget::doubleClicked, this, &OrderDlg::onAdd2Right);
	connect(mTreeRight, &QTreeWidget::doubleClicked, this, &OrderDlg::onRemoveFromRight);
}

void N_ORDERSYS::OrderDlg::initFB()
{
	this->resize(1000, 700);

	if (this->objectName().isEmpty())
		this->setObjectName(QString::fromUtf8("OrderDlg"));

	auto verticalLayout_3 = new QVBoxLayout(this);
	auto verticalLayout_2 = new QVBoxLayout();
	auto horizontalLayout_2 = new QHBoxLayout();

	mTreeLeft = new QTreeWidget(this);
	horizontalLayout_2->addWidget(mTreeLeft);
	mTreeLeft->setHeaderLabels(QStringList() << QString::fromLocal8Bit("编号") 
		<< QString::fromLocal8Bit("菜名")
		<< QString::fromLocal8Bit("价格") 
		<< QString::fromLocal8Bit("库存剩余"));

	auto verticalLayout = new QVBoxLayout();
	mBtn2Right = new QPushButton(QString::fromLocal8Bit(">>"), this);
	mBtn2Right->setObjectName(QString::fromUtf8("mBtn2Right"));
	mBtn2Right->setMinimumHeight(mBtn2Right->width());
	verticalLayout->addWidget(mBtn2Right);

	mBtn2Left = new QPushButton(QString::fromLocal8Bit("<<"), this);
	mBtn2Left->setObjectName(QString::fromUtf8("mBtn2Left"));
	mBtn2Left->setMinimumHeight(mBtn2Left->width());
	verticalLayout->addWidget(mBtn2Left);

	horizontalLayout_2->addLayout(verticalLayout);


	mTreeRight = new QTreeWidget(this);
	horizontalLayout_2->addWidget(mTreeRight);
	mTreeRight->setHeaderLabels(QStringList() << QString::fromLocal8Bit("编号") 
		<< QString::fromLocal8Bit("菜名")
		<< QString::fromLocal8Bit("价格")
		<< QString::fromLocal8Bit("数量"));

	verticalLayout_2->addLayout(horizontalLayout_2);


	auto horizontalLayout = new QHBoxLayout();
	horizontalLayout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

	mBtnOk = new QPushButton(QString::fromLocal8Bit("确定"),this);
	horizontalLayout->addWidget(mBtnOk);

	pushCancel = new QPushButton(QString::fromLocal8Bit("取消"), this);
	horizontalLayout->addWidget(pushCancel);

	pushApply = new QPushButton(QString::fromLocal8Bit("应用"), this);
	horizontalLayout->addWidget(pushApply);

	verticalLayout_2->addLayout(horizontalLayout);


	verticalLayout_3->addLayout(verticalLayout_2);

	mTreeLeft->addTopLevelItems(QList<QTreeWidgetItem*>()
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("小菜"))
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("主菜"))
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("饮料"))
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("其他"))
	);

	mTreeLeft->expandAll();
}

void N_ORDERSYS::OrderDlg::initValue()
{
	//从数据库取数据填充菜单
	//auto dishs=SupportDishList::instance().getLstSupportDishList();

	//for (auto &itTuple : dishs) {
	//	int  type = static_cast<int>(itTuple.first);
	//	for (auto &it : itTuple.second) {
	//		QString id = QString::number(it.getId());
	//		QString name = it.getDishName().c_str();
	//		QString price = QString::number(it.getPrice());
	//		QString count = QString::number(it.getCount());
	//		auto item = new QTreeWidgetItem(QStringList() <<id<< name << price << count);
	//		mTreeLeft->topLevelItem(type)->addChild(item);
	//	}
	//}

	reflushTreeInfo(mTreeLeft, mLeftList);
	reflushTreeInfo(mTreeRight, mRightList);
}

void N_ORDERSYS::OrderDlg::reflushTreeInfo(QTreeWidget*  tree, const DishList & lst)
{
	tree->clear();
	tree->addTopLevelItems(QList<QTreeWidgetItem*>()
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("小菜"))
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("主菜"))
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("饮料"))
		<< new QTreeWidgetItem(QStringList() << QString::fromLocal8Bit("其他"))
	);

	//从订单填充mTreeRight
	auto lm = [=](const CookDish& dish) {
		int  type = static_cast<int>(CookDish::getCookType(dish));
		QString id = QString::number(dish.getId());
		QString name =QString::fromUtf8( dish.getDishName().c_str());//从数据中读取的数据是utf-8，没有转换，所以这里不用fromlocalutf-8
		QString price = QString::number(dish.getPrice());
		QString count = QString::number(dish.getCount());
		auto item = new QTreeWidgetItem(QStringList() << id << name << price << count);
		tree->topLevelItem(type)->addChild(item);

		if (dish.getId() == mPreSelectId) {
			tree->setItemSelected(item,true);
		}
	};

	lst.for_each(lm);
	tree->expandAll();

	//BillingDetails
}

void N_ORDERSYS::OrderDlg::onAdd2Right()
{
	auto selectItems=mTreeLeft->selectedItems();
	if (selectItems.size() != 1)
		return;
	auto childCount=selectItems[0]->childCount();
	//设置只能选择一项，所以必须是0
	auto id=selectItems[0]->text(0).toInt();
	//如果没有子节点并且id不等于0说明是最后一项菜单，并且数量不能为0
	if (childCount == 0 && id != 0 && mLeftList.getDish(id).getCount() != 0) {
		mPreSelectId = id;
		//添加到mode，并刷新右边
		mRightList.addDish(CookDish(id));
		mLeftList.removeDish(CookDish(id));
		reflushTreeInfo(mTreeRight, mRightList);
		reflushTreeInfo(mTreeLeft, mLeftList);
	}

}

void N_ORDERSYS::OrderDlg::onRemoveFromRight()
{
	auto selectItems = mTreeRight->selectedItems();
	if (selectItems.size() != 1)
		return;
	auto childCount = selectItems[0]->childCount();
	//设置只能选择一项，所以必须是0
	auto id = selectItems[0]->text(0).toInt();
	//如果没有子节点并且id不等于0说明是最后一项菜单
	if (childCount == 0 && id != 0) {
		mPreSelectId = id;
		//从mode删除，并刷新右边
		mRightList.removeDish(CookDish(id));
		mLeftList.addDish(CookDish(id));
		reflushTreeInfo(mTreeRight, mRightList);
		reflushTreeInfo(mTreeLeft, mLeftList);
	}
}

void N_ORDERSYS::OrderDlg::onApplyOrder()
{
	//遍历点单，弹出统计确认，如果Ok，保存，不ok返回

}

void N_ORDERSYS::OrderDlg::onOkOrder()
{
	//开始是灰色的，等到apply之后可以点击。
	QDialog::accept();
}

N_ORDERSYS::OrderDishDlg::OrderDishDlg(DishList::ptr lst, DishList::ptr rst)
	:OrderDlg(lst,rst)
{
	this->setWindowTitle(QString::fromLocal8Bit("点餐界面"));
}

N_ORDERSYS::OrderDishDlg::OrderDishDlg(const DishList & lst, const DishList & rst)
	: OrderDlg(lst, rst)
{
	this->setWindowTitle(QString::fromLocal8Bit("点餐界面"));
}

N_ORDERSYS::OrderDishDlg::~OrderDishDlg()
{
}

N_ORDERSYS::UpDishDlg::UpDishDlg(DishList::ptr lst, DishList::ptr rst)
	: OrderDlg(lst, rst)
{
	this->setWindowTitle(QString::fromLocal8Bit("上菜界面"));
	mBtn2Left->setEnabled(false);
}

N_ORDERSYS::UpDishDlg::UpDishDlg(const DishList & lst, const DishList & rst)
	: OrderDlg(lst, rst)
{
	this->setWindowTitle(QString::fromLocal8Bit("上菜界面"));
	mBtn2Left->setEnabled(false);
}

N_ORDERSYS::UpDishDlg::~UpDishDlg()
{
}
