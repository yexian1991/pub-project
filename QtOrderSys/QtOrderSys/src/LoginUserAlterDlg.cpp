﻿#include "LoginUserAlterDlg.h"
#include "LoginUserLmt.h"
#include "qboxlayout.h"
#include "qgridlayout.h"
#include "qmessagebox.h"
#include "databaseOperatoe.h"
#include "log.h"


N_ORDERSYS::LoginUserAlterDlg::LoginUserAlterDlg()
{
}

N_ORDERSYS::LoginUserAlterDlg::LoginUserAlterDlg(const LoginUser & user)
	:mOldUser(user)
{
}

void N_ORDERSYS::LoginUserAlterDlg::initFB()
{
	this->resize(600, 380);

	if (this->objectName().isEmpty())
		this->setObjectName(QString::fromUtf8("LoginUserAlterDlg"));

	auto horizontalLayout_9 = new QHBoxLayout(this);
	auto horizontalLayout_8 = new QHBoxLayout();

	auto mGBAccount = new QGroupBox(QString::fromLocal8Bit("账户信息"),this);
	auto verticalLayout_4 = new QVBoxLayout(mGBAccount);

	auto gridLayout = new QGridLayout();
	QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);

#define INITGRIDLAYOUT(LABELNAME,EDITNAME,LABDISPLAY,R,C,RW,CW)\
	auto horizontalLayout##LABELNAME = new QHBoxLayout();\
	auto LABELNAME = new QLabel(mGBAccount);\
	LABELNAME->setText(QString::fromLocal8Bit(LABDISPLAY));\
	horizontalLayout##LABELNAME->addWidget(LABELNAME);\
	EDITNAME = new QLineEdit(mGBAccount);\
	sizePolicy.setHeightForWidth(EDITNAME->sizePolicy().hasHeightForWidth());\
	EDITNAME->setSizePolicy(sizePolicy);\
	horizontalLayout##LABELNAME->addWidget(EDITNAME);\
	gridLayout->addLayout(horizontalLayout##LABELNAME, R,C,RW,CW);\


	INITGRIDLAYOUT(mLabAccount, mEdtAccount, "账号", 0, 0, 1, 2);
	INITGRIDLAYOUT(mLabName, mEdtName, "姓名", 1, 0, 1, 2);
	INITGRIDLAYOUT(mLabPwd, mEdtPwd, "密码", 2, 0, 1, 2);
	mEdtPwd->setEchoMode(QLineEdit::PasswordEchoOnEdit);
	INITGRIDLAYOUT(mLabPhone, mEdtPhone, "电话", 3, 0, 1, 2);
	INITGRIDLAYOUT(mLabAddr, mEdtAddr, "地址", 5, 0, 1, 3);

#undef INITGRIDLAYOUT

	auto horizontalLayout_6 = new QHBoxLayout();
	auto mLabAge = new QLabel(mGBAccount);
	mLabAge->setText(QString::fromLocal8Bit("年龄"));
	horizontalLayout_6->addWidget(mLabAge);
	mSBAge = new QSpinBox(mGBAccount);
	sizePolicy.setHeightForWidth(mSBAge->sizePolicy().hasHeightForWidth());
	mSBAge->setSizePolicy(sizePolicy);
	horizontalLayout_6->addWidget(mSBAge);
	gridLayout->addLayout(horizontalLayout_6, 4, 0, 1, 2);

	//
	auto mGBSex = new QGroupBox(QString::fromLocal8Bit("性别"),mGBAccount);
	auto verticalLayout_2 = new QVBoxLayout(mGBSex);
	auto verticalLayout = new QVBoxLayout();
	mRBfemale = new QRadioButton(QString::fromLocal8Bit("女"),mGBSex);
	verticalLayout->addWidget(mRBfemale);
	mRBmale = new QRadioButton(QString::fromLocal8Bit("男"),mGBSex);
	verticalLayout->addWidget(mRBmale);
	verticalLayout_2->addLayout(verticalLayout);
	gridLayout->addWidget(mGBSex, 0, 2, 2, 1);
	//
	auto mGBLmt = new QGroupBox(QString::fromLocal8Bit("权限"), mGBAccount);
	auto verticalLayout_5 = new QVBoxLayout(mGBLmt);
	auto verticalLayout_mGBLmt = new QVBoxLayout();
	
	mCBdinner = new QCheckBox(QString::fromLocal8Bit("点餐权限"), mGBLmt);
	verticalLayout_mGBLmt->addWidget(mCBdinner);

	mCBbill = new QCheckBox(QString::fromLocal8Bit("财务权限"), mGBLmt);
	verticalLayout_mGBLmt->addWidget(mCBbill);

	mCBfood = new QCheckBox(QString::fromLocal8Bit("厨师长权限"), mGBLmt);
	verticalLayout_mGBLmt->addWidget(mCBfood);

	mCBhr = new QCheckBox(QString::fromLocal8Bit("人事权限"), mGBLmt);
	verticalLayout_mGBLmt->addWidget(mCBhr);

	verticalLayout_5->addLayout(verticalLayout_mGBLmt);
	gridLayout->addWidget(mGBLmt, 2, 2, 3, 1);
	//

	verticalLayout_4->addLayout(gridLayout);
	horizontalLayout_8->addWidget(mGBAccount);

	//
	auto verticalLayout_3 = new QVBoxLayout();
	auto verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);
	verticalLayout_3->addItem(verticalSpacer_2);
	mBtnOk = new QPushButton(QString::fromLocal8Bit("确定"),this);
	mBtnOk->setObjectName(QString::fromUtf8("mBtnOk"));
	QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
	sizePolicy1.setHorizontalStretch(0);
	sizePolicy1.setVerticalStretch(0);
	sizePolicy1.setHeightForWidth(mBtnOk->sizePolicy().hasHeightForWidth());
	mBtnOk->setSizePolicy(sizePolicy1);
	verticalLayout_3->addWidget(mBtnOk);

	mBtnCancel = new QPushButton(QString::fromLocal8Bit("取消"), this);
	mBtnCancel->setObjectName(QString::fromUtf8("mBtnCancel"));
	sizePolicy1.setHeightForWidth(mBtnCancel->sizePolicy().hasHeightForWidth());
	mBtnCancel->setSizePolicy(sizePolicy1);
	verticalLayout_3->addWidget(mBtnCancel);
	auto verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
	verticalLayout_3->addItem(verticalSpacer);

	//
	horizontalLayout_8->addLayout(verticalLayout_3);
	horizontalLayout_9->addLayout(horizontalLayout_8);
}

void N_ORDERSYS::LoginUserAlterDlg::initValue()
{
	mEdtPwd->setText(QString::fromStdString(mOldUser.getPassword()));
	mEdtAddr->setText(QString::fromStdString(mOldUser.getAdder()));
	mEdtPhone->setText(QString::fromStdString(mOldUser.getPhone()));
	mEdtAccount->setText(QString::fromStdString(mOldUser.getAccount()));
	mEdtName->setText(QString::fromStdString(mOldUser.getName()));
	mSBAge->setValue(mOldUser.getAge());

	LoginUserLmt lmt(mOldUser.getLmt());
	mCBdinner->setChecked(lmt.hasLmt(0));//显示项和01234是一对一的关系，所以没有关系
	mCBbill->setChecked(lmt.hasLmt(1));			//这里的列表应该是动态加载的
	mCBfood->setChecked(lmt.hasLmt(2));
	mCBhr->setChecked(lmt.hasLmt(3));

	mRBfemale->setChecked(mOldUser.isFemale());
	mRBmale->setChecked(!mOldUser.isFemale());
}

void N_ORDERSYS::LoginUserAlterDlg::bindOperator()
{
	connect(mBtnOk, &QPushButton::clicked, this, &QDialog::accept);
	connect(mBtnCancel, &QPushButton::clicked, this, &QDialog::reject);
}

void N_ORDERSYS::LoginUserAlterDlg::setCtrlValidator()
{
	mEdtPwd->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z0-9]{4,20}"), this));
	mEdtPhone->setValidator(new QRegExpValidator(QRegExp("[0-9]{11}"), this));
	mEdtAccount->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z0-9]{4,20}"), this));
}

N_ORDERSYS::LoginUser N_ORDERSYS::LoginUserAlterDlg::getValue() const
{
	std::string pwd = mEdtPwd->text().toStdString();
	std::string addr=mEdtAddr->text().toStdString();
	std::string phone=mEdtPhone->text().toStdString();
	std::string account=mEdtAccount->text().toStdString();
	std::string name=mEdtName->text().toStdString();
	int age=mSBAge->value();
	int sex = mRBfemale->isChecked() ? 0 : 1;

	LoginUserLmt lmt;
	lmt.setLmt(0, mCBdinner->isChecked());
	lmt.setLmt(1, mCBbill->isChecked());
	lmt.setLmt(2, mCBfood->isChecked());
	lmt.setLmt(3, mCBhr->isChecked());
	int lmtValue = lmt.getValue();

	N_ORDERSYS::LoginUser user{ account,name,pwd,phone,age,sex,addr,lmtValue };
	return user;
}

void N_ORDERSYS::LoginUserAlterDlg::accept()
{
	auto ret = getValue().isPerfect();
	if (false == ret.first) {
		QMessageBox::warning(this, "tips", QString::fromLocal8Bit(ret.second.c_str()));
		return;
	}

	QDialog::accept();
}

void N_ORDERSYS::LoginUserAlterDlg::create()
{
	initFB();
	initValue();
	bindOperator();
	initDlgState();
}


/////////////////////////
N_ORDERSYS::LoginUserAlterDlgAlter::LoginUserAlterDlgAlter(const LoginUser & user)
	: LoginUserAlterDlg(user)
{
	create();
}

void N_ORDERSYS::LoginUserAlterDlgAlter::initDlgState()
{
	this->setWindowTitle(QString::fromLocal8Bit("更新用户信息"));
	disableCtrl();
}

void N_ORDERSYS::LoginUserAlterDlgAlter::bindOperator()
{
	LoginUserAlterDlg::bindOperator();
	connect(mEdtName, &QLineEdit::editingFinished, [=]() {disableCtrl(); });
	connect(mEdtPwd, &QLineEdit::editingFinished, [=]() {disableCtrl(); });
	connect(mEdtAddr, &QLineEdit::editingFinished, [=]() {disableCtrl(); });
	connect(mEdtPhone, &QLineEdit::editingFinished, [=]() {disableCtrl(); });
	connect(mSBAge, &QSpinBox::editingFinished, [=]() {disableCtrl(); });
	connect(mRBfemale, &QRadioButton::clicked, [=]() {disableCtrl(); });
	connect(mRBmale, &QRadioButton::clicked, [=]() {disableCtrl(); });

	connect(mCBdinner, &QCheckBox::clicked, [=]() {disableCtrl(); });
	connect(mCBbill, &QCheckBox::clicked, [=]() {disableCtrl(); });
	connect(mCBfood, &QCheckBox::clicked, [=]() {disableCtrl(); });
	connect(mCBhr, &QCheckBox::clicked, [=]() {disableCtrl(); });
}

void N_ORDERSYS::LoginUserAlterDlgAlter::disableCtrl()
{
	mEdtAccount->setEnabled(false);
	mBtnOk->setEnabled(!(getValue() == mOldUser));
}

/////////////////////////
N_ORDERSYS::LoginUserAlterDlgAdd::LoginUserAlterDlgAdd():LoginUserAlterDlg()
{
	create();
}

void N_ORDERSYS::LoginUserAlterDlgAdd::initDlgState()
{
	this->setWindowTitle(QString::fromLocal8Bit("增加用户信息"));
}

/////////////////////////
N_ORDERSYS::LoginUserAlterDlgCheck::LoginUserAlterDlgCheck(const LoginUser & user)
	: LoginUserAlterDlg(user)
{
	create();
}

void N_ORDERSYS::LoginUserAlterDlgCheck::initDlgState()
{
	this->setWindowTitle(QString::fromLocal8Bit("查看用户信息"));
	disableCtrl();
}

void N_ORDERSYS::LoginUserAlterDlgCheck::disableCtrl()
{
	mEdtPwd->setEnabled(false);
	mEdtAddr->setEnabled(false);
	mEdtPhone->setEnabled(false);
	mEdtAccount->setEnabled(false);
	mEdtName->setEnabled(false);
	mSBAge->setEnabled(false);
	mRBfemale->setEnabled(false);
	mRBmale->setEnabled(false);
	mCBdinner->setEnabled(false);
	mCBbill->setEnabled(false);
	mCBfood->setEnabled(false);
	mCBhr->setEnabled(false);
}
