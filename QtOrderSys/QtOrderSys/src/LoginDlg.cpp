﻿#include "LoginDlg.h"
#include "LoginUserDbOperator.h"
#include "databaseOperatoe.h"
#include "qmessagebox.h"
using namespace N_ORDERSYS;


LoginDlg::LoginDlg()
{
}


LoginDlg::LoginDlg(QWidget * parent, Qt::WindowFlags f):QDialog(parent,f)
{
	this->setWindowTitle(QString::fromLocal8Bit("用户登录窗口"));
	initFB();
	
	connect(mBtnOk, &QPushButton::clicked, this, &QDialog::accept);
	connect(mBtnCancel, &QPushButton::clicked, this, &QDialog::reject);
}

LoginDlg::~LoginDlg()
{
}

void LoginDlg::initFB()
{
	if (this->objectName().isEmpty())
		this->setObjectName(QString::fromUtf8("Dialog"));
	this->resize(505, 252);
	gridLayout_2 = new QGridLayout(this);
	gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
	horizontalLayout_4 = new QHBoxLayout();
	horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
	widget = new QWidget(this);
	widget->setObjectName(QString::fromUtf8("widget"));
	QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);
	sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
	widget->setSizePolicy(sizePolicy);
	widget->setMinimumSize(QSize(200, 200));
	widget->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 85, 255);"));

	horizontalLayout_4->addWidget(widget);

	verticalLayout = new QVBoxLayout();
	verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
	verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	verticalLayout->addItem(verticalSpacer);

	horizontalLayout = new QHBoxLayout();
	horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
	label = new QLabel(this);
	label->setObjectName(QString::fromUtf8("label"));

	horizontalLayout->addWidget(label);

	mUserName = new QLineEdit(this);
	mUserName->setObjectName(QString::fromUtf8("mUserName"));

	horizontalLayout->addWidget(mUserName);


	verticalLayout->addLayout(horizontalLayout);

	horizontalLayout_2 = new QHBoxLayout();
	horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
	label_2 = new QLabel(this);
	label_2->setObjectName(QString::fromUtf8("label_2"));

	horizontalLayout_2->addWidget(label_2);

	mUserPassword = new QLineEdit(this);
	mUserPassword->setObjectName(QString::fromUtf8("mUserPassword"));
	mUserPassword->setEchoMode(QLineEdit::Password);

	horizontalLayout_2->addWidget(mUserPassword);


	verticalLayout->addLayout(horizontalLayout_2);

	horizontalLayout_3 = new QHBoxLayout();
	horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
	horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	horizontalLayout_3->addItem(horizontalSpacer);

	mBtnOk = new QPushButton(this);
	mBtnOk->setObjectName(QString::fromUtf8("pushButton"));

	horizontalLayout_3->addWidget(mBtnOk);

	mBtnCancel = new QPushButton(this);
	mBtnCancel->setObjectName(QString::fromUtf8("pushButton_2"));

	horizontalLayout_3->addWidget(mBtnCancel);


	verticalLayout->addLayout(horizontalLayout_3);

	verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

	verticalLayout->addItem(verticalSpacer_2);


	horizontalLayout_4->addLayout(verticalLayout);


	gridLayout_2->addLayout(horizontalLayout_4, 0, 0, 1, 1);

	label->setText(QString::fromLocal8Bit("账户："));
	label_2->setText(QString::fromLocal8Bit("密码："));
	mBtnOk->setText(QString::fromLocal8Bit("确定"));
	mBtnCancel->setText(QString::fromLocal8Bit("退出"));
}

///
/// @param name 登录账户
/// @param pwd	登录密码
/// @return 判断值
LoginErrorType N_ORDERSYS::LoginDlg::judgeUserPwd(const std::string & name, const std::string & pwd)
{
	LoginUserDbOperator dbop{ N_ORDERSYS::DataBaseOperator::instance().getDBConnect() };
	if (name.size() == 0)
		return LoginErrorType::LE_USERSTRSPACE;
	if (name.size() >20)
		return LoginErrorType::LE_USERSTRERROR;
	if (false == dbop.hasUser(name))
		return LoginErrorType::LE_NOUSER;
	if (pwd.size() == 0)
		return LoginErrorType::LE_PWDSTRSPACE;
	if (pwd.size() > 20)
		return LoginErrorType::LE_PWDSTRERROR;
	if (false == dbop.isRightpwd(name, pwd))
		return LoginErrorType::LE_ERRORPWD;

	return LoginErrorType::LE_SUCESS;
}

void N_ORDERSYS::LoginDlg::accept()
{
	auto name=mUserName->text().toStdString();
	auto pwd = mUserPassword->text().toStdString();

	switch (judgeUserPwd(name, pwd))
	{
	case LoginErrorType::LE_SUCESS:
		QMessageBox::about(this, "tips", QString::fromLocal8Bit("登录成功！"));
		QDialog::accept();
		break;
	case LoginErrorType::LE_USERSTRSPACE:
		QMessageBox::warning(this, "tips", QString::fromLocal8Bit("用户名不能为空！"));
		break;
	case LoginErrorType::LE_USERSTRERROR:
		QMessageBox::warning(this, "tips", QString::fromLocal8Bit("用户名格式不对！"));
		break;
	case LoginErrorType::LE_NOUSER:
		QMessageBox::warning(this, "tips", QString::fromLocal8Bit("用户不存在，请检查！"));
		break;
	case LoginErrorType::LE_PWDSTRSPACE:
		QMessageBox::warning(this, "tips", QString::fromLocal8Bit("密码不能为空！"));
		break;
	case LoginErrorType::LE_PWDSTRERROR:
		QMessageBox::warning(this, "tips", QString::fromLocal8Bit("密码格式不对！"));
		break;
	case LoginErrorType::LE_ERRORPWD:
		QMessageBox::warning(this, "tips", QString::fromLocal8Bit("密码错误！"));
		break;
	default:
		break;
	}

}
