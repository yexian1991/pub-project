﻿#include "DishList.h"
#include"CookDishDbOperator.h"
#include "databaseOperatoe.h"
#include  <assert.h>

using namespace N_ORDERSYS;

DishList::DishList()
{
}

N_ORDERSYS::DishList::DishList(const CookListMap & lst)
{
	mDishList = lst;
}

N_ORDERSYS::DishList::DishList(const DishList & ths)
{
	*this = ths;
}

N_ORDERSYS::DishList::DishList(const DishList && ths)
{
	*this = std::move(ths);
}


DishList::~DishList()
{
}

DishList& N_ORDERSYS::DishList::operator=(const DishList & ths)
{
	mDishList = ths.mDishList;
	return *this;
}

DishList& N_ORDERSYS::DishList::operator=(const DishList && ths)
{
	mDishList = std::move( ths.mDishList);
	return *this;
}

bool N_ORDERSYS::DishList::operator==(const DishList & ths)
{
	//return mDishList == ths.mDishList;//会调用CookDish的重载==，而其只比较id，导致错误

	bool isequal = true;
	this->for_each([&](const CookDish&dish) {
		if (dish.getCount() != ths.getDish(dish.getId()).getCount()) {
			isequal = false;
			return;
		}
	});
	return isequal;
}

bool N_ORDERSYS::DishList::operator!=(const DishList & ths)
{
	return (*this == ths) == false;
}


//DishList N_ORDERSYS::DishList::operator+(const DishList & ths)
//{
//	// 新的列表
//	DishList sumlst;
//	this->for_each([&](const CookDish& dish) {
//		sumlst.addDish(dish);
//	});
//
//	ths.for_each([&](const CookDish& dish) {
//		sumlst.addDish(dish);
//	});
//
//	*this = sumlst;
//	return *this;
//}

bool N_ORDERSYS::DishList::isEmpty() const
{
	return mDishList.empty();
}

bool N_ORDERSYS::DishList::hasDish(int id) const
{
	bool has = false;
	this->for_each([&](const CookDish&dish) {
		if (dish.getId() == id)
			has = true;
	});
	return has;
}

bool N_ORDERSYS::DishList::hasDish(const CookDish & dish) const
{
	return hasDish(dish.getId());
}

CookDish N_ORDERSYS::DishList::getDish(int id) const
{
	if (hasDish(id) == false)
		return CookDish();

	CookDish dh;
	this->for_each([&](const CookDish&dish) {
		if (dish.getId() == id)
			dh = dish;
	});
	return dh;
}

CookDish N_ORDERSYS::DishList::getDish(const CookDish & dish) const
{
	return getDish(dish.getId());
}

void N_ORDERSYS::DishList::addDish(const CookDish & dish, int count)
{
	auto& typeVct = mDishList[dish.getType()];

	for (auto it = typeVct.begin(); it != typeVct.end(); ++it) {
		if (it->isSameId(dish) == true) {
			it->incCount(count);
			return ;
		}
	}

	typeVct.emplace_back(dish);
	return;
}

bool N_ORDERSYS::DishList::removeDish(const CookDish & dish, int count)
{
	auto& typeVct = mDishList[dish.getType()];

	for (auto it = typeVct.begin(); it != typeVct.end(); ++it) {
		if (it->isSameId(dish) == true) {
			it->decCount(count);
			if (it->getCount() <= 0)
				typeVct.erase(it);
			return true;
		}
	}
	return false;
}

std::string N_ORDERSYS::DishList::getOrderListStr_u8()
{
	//细化成格式化输出
	std::ostringstream ss;
	ss << u8"当前已点菜品：" << std::endl;

	auto lm = [&](const CookDish&dish) {
		ss << "\t" << dish.getDishName().c_str() <<"\t"<<dish.getCount()<< std::endl;
	};

	this->for_each(lm);
	return ss.str();
}

void N_ORDERSYS::DishList::for_each(std::function<void(CookDish&dish)> fun) const 
{
	if (this->mDishList.size() <= 0)
		return;
	for (auto itTuple : mDishList) {
		for (auto it : itTuple.second) {
			fun(it);
		}
	}
}



bool N_ORDERSYS::SupportDishList::init()
{
	CookDishDbOperator opt{ DataBaseOperator::instance().getDBConnect() };

	if (opt.lookStaff(mDishList) == false) {
		OLOG_E() << "从数据库获取菜单失败";
		return  false;
	}
	return  true;
}

DishList::ptr N_ORDERSYS::SupportDishList::getSupportDishListCopy()
{
	return std::make_shared<DishList>(this->mDishList);
}

void N_ORDERSYS::SupportDishList::setSupportDishList(const DishList& p)
{
	mDishList = p.getOrderList();
}

N_ORDERSYS::SupportDishList::~SupportDishList()
{
}


//DishList N_ORDERSYS::SupportDishList::getLstSupportDishList()
//{
//	CookDishDbOperator opt{ DataBaseOperator::instance().getDBConnect() };
//	decltype(mDishList) tDishList;
//	if (opt.lookStaff(tDishList) == false) {
//		OLOG_E() << "从数据库获取菜单失败";
//		return  *this;
//	}
//	return tDishList;
//}

DishList N_ORDERSYS::operator-(const DishList & a, const DishList & b)
{
	//如果a无b有需要报错，a有b有相减小于0不报错
	DishList k(a);

	b.for_each([&](CookDish& dish) {
		if (false == k.removeDish(dish, dish.getCount())) {
			//如果a中没有
			OLOG_E() << "菜单相减时候不能用少的减多的";
				throw(std::exception("cant use operator- because left is less"));
		}
	});
	return k;
}

DishList N_ORDERSYS::operator+(const DishList & a, const DishList & b)
{
	DishList k(a);
	//遍历b
	b.for_each([&](CookDish& dish) {
		k.addDish(dish, dish.getCount());
	});
	return k;
}
