﻿#include "TableState.h"
#include "qdialog.h"
using namespace N_ORDERSYS;


TableState::TableState()
{
}


TableState::~TableState()
{
}


N_ORDERSYS::TableStateContext::TableStateContext(TableState::ptr pState)
{
	mStatePtr = pState;
}


void N_ORDERSYS::TableStateContext::next()
{
	if (nullptr != this->mStatePtr)
	{
		this->mStatePtr->Handle(this);
	}
}

std::map<std::chrono::system_clock::time_point, TableState::ptr> N_ORDERSYS::TableStateContext::getHisTableState() const
{
	return mHisTableState;
}

void N_ORDERSYS::TableStateContext::changeState(TableState::ptr pState)
{
	mHisTableState[std::chrono::system_clock::now()]=this->mStatePtr;
	this->mStatePtr = pState;
}

bool N_ORDERSYS::TableStateContext::isReady()
{
	return true;
}

EN_TableState N_ORDERSYS::TableStateContext::getStateNow() const
{
	return mStatePtr->getStateNow();
}

std::string N_ORDERSYS::TableStateContext::getStateNowStr() const
{
	return mStatePtr->getStateNowStr();
}


void N_ORDERSYS::TableState_ok::Handle(TableStateContext * pContext)
{
	//do
	pContext->changeState(std::make_shared<TableState_ordering>());
}

EN_TableState N_ORDERSYS::TableState_ok::getStateNow() const
{
	return EN_TableState::TS_OK;
}

std::string N_ORDERSYS::TableState_ok::getStateNowStr() const
{
	return std::string("OK");
}

void N_ORDERSYS::TableState_ordering::Handle(TableStateContext * pContext)
{	
	//do
	pContext->changeState(std::make_shared<TableState_cooking>());
}

EN_TableState N_ORDERSYS::TableState_ordering::getStateNow() const
{
	return EN_TableState::TS_ORDERING;
}

std::string N_ORDERSYS::TableState_ordering::getStateNowStr() const
{
	return std::string("TS_ORDERING");
}

void N_ORDERSYS::TableState_cooking::Handle(TableStateContext * pContext)
{
	//do
	pContext->changeState(std::make_shared<TableState_uping>());
}

EN_TableState N_ORDERSYS::TableState_cooking::getStateNow() const
{
	return EN_TableState::TS_COOKING;
}

std::string N_ORDERSYS::TableState_cooking::getStateNowStr() const
{
	return std::string("TS_COOKING");
}

void N_ORDERSYS::TableState_uping::Handle(TableStateContext * pContext)
{
	//do
	if (pContext->isReady())
		pContext->changeState(std::make_shared<TableState_serving>());
}

EN_TableState N_ORDERSYS::TableState_uping::getStateNow() const
{
	return EN_TableState::TS_UPING;
}

std::string N_ORDERSYS::TableState_uping::getStateNowStr() const
{
	return std::string("TS_UPING");
}

void N_ORDERSYS::TableState_serving::Handle(TableStateContext * pContext)
{
	//do
	pContext->changeState(std::make_shared<TableState_cleaning>());
}

EN_TableState N_ORDERSYS::TableState_serving::getStateNow() const
{
	return EN_TableState::TS_SERVING;
}

std::string N_ORDERSYS::TableState_serving::getStateNowStr() const
{
	return std::string("TS_SERVING");
}

void N_ORDERSYS::TableState_cleaning::Handle(TableStateContext * pContext)
{
	//do
	pContext->changeState(std::make_shared<TableState_ok>());
}

EN_TableState N_ORDERSYS::TableState_cleaning::getStateNow() const
{
	return EN_TableState::TS_CLEANING;
}

std::string N_ORDERSYS::TableState_cleaning::getStateNowStr() const
{
	return std::string("TS_CLEANING");
}

void N_ORDERSYS::TableStateMap::init()
{
	if (fromFileCfg() == false)
		defaultCfg();
}

TableStateCell N_ORDERSYS::TableStateMap::get(EN_TableState state) const
{
	if (mData.find(state) == mData.end())
		return TableStateCell();
	return mData.at(state);
}

int N_ORDERSYS::TableStateMap::getTableColor(EN_TableState state) const
{
	if (mData.find(state) == mData.end())
		return 0;
	return mData.at(state).mColor;
}

bool N_ORDERSYS::TableStateMap::fromFileCfg()
{
	return false;
}

void N_ORDERSYS::TableStateMap::defaultCfg()
{
	mData[EN_TableState::TS_OK] = TableStateCell{ EN_TableState::TS_OK ,0xffffff ,"TS_OK"};
	mData[EN_TableState::TS_ORDERING] = TableStateCell{ EN_TableState::TS_ORDERING ,0xff7f00  ,"TS_ORDERING" };
	mData[EN_TableState::TS_COOKING] = TableStateCell{ EN_TableState::TS_COOKING ,0xffff00  ,"TS_COOKING" };
	mData[EN_TableState::TS_UPING] = TableStateCell{ EN_TableState::TS_UPING ,0x00ff00  ,"TS_UPING" };
	mData[EN_TableState::TS_SERVING] = TableStateCell{ EN_TableState::TS_SERVING ,0x0000ff  ,"TS_SERVING" };
	mData[EN_TableState::TS_CLEANING] = TableStateCell{ EN_TableState::TS_CLEANING ,0x8b00ff  ,"TS_CLEANING" };
}