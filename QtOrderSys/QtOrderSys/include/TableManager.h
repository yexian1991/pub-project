﻿#pragma once
#include <qobject.h>
#include "TableState.h"
#include "BillingDetails.h"
#include "CustomInfo.h"


namespace N_ORDERSYS {
	//当前有哪些桌子，各个桌子的规格应该从数据库取
	//在初始化程序的时候完成以上动作，在升级版中可以用户手动设置规格，设置各桌参数
	//通过TableListWidget打开的桌子就是本处的对象，事先已经实例化。

	//此时主要功能，查看桌子状态，设置桌子状态，服务桌子承载的客户
}


//餐桌系统，
namespace N_ORDERSYS {

	/**
	*	@brief 一次服务的综合管理对象，内部维护一个服务对象，客户对象，账单对象
	*
	*	服务对象TableStateContext，负责一次服务的生命周期
	*	客户对象CustomInfo，负责客户的维护
	*	订单对象BillingDetails，负责对订单的维护
	*/
	class TableManager :public QObject
	{
	public:

		enum TableMounment {
			TM_SMALL,
			TM_MID,
			TM_BIG,
			TM_BIGER
		};
	public:
		using ptr = std::shared_ptr<TableManager>;

		TableManager();
		TableManager(int tableID,TableMounment);
		~TableManager();
	public:
		void reset();
		EN_TableState getStateNow() const;
		TableStateContext::ptr getTableStateContext() { return mTableStatePtr; }
		CustomInfo::ptr getCustomInfo() { return mCustomInfo; }
		BillingDetails::ptr getBillingDetails() { return mBillingDetails; }
	private:
		int mTableID{ -1 };			///<餐桌号
		///餐桌类型
		TableMounment mTableMounment{ TableMounment::TM_SMALL };

		TableStateContext::ptr  mTableStatePtr{ nullptr };		///<服务对象
		CustomInfo::ptr mCustomInfo{ nullptr };						///<客户对象
		BillingDetails::ptr mBillingDetails{ nullptr };					///<订单对象
	};
}
