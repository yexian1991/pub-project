﻿#pragma once
#include <qdialog.h>
#include "qtreewidget.h"
#include "qpushbutton.h"
#include "DishList.h"

namespace N_ORDERSYS
{

	/**
	*	@brief OrderDlg是两树的基类，可以做点菜用，也可以做上菜用，还可以做加减菜用
	*
	*	this class
	*/
	class OrderDlg :public QDialog
	{
	public:
		/// @brief 传入左右两个列表数据
		OrderDlg(DishList::ptr lst,DishList::ptr rst);
		OrderDlg(const DishList& lst, const DishList& rst);
		virtual ~OrderDlg();

	protected:
		DishList getRightDishList() { return mRightList; }
		DishList getLeftDishList() { return mLeftList; }
	private:
		void create();
		void initFB();
		void initValue();
		void reflushTreeInfo(QTreeWidget*  tree, const DishList & lst);

		void onAdd2Right();
		void onRemoveFromRight();
		void onApplyOrder();
		void onOkOrder();

	protected:
		QTreeWidget *mTreeLeft;
		QTreeWidget *mTreeRight;

		QPushButton *mBtn2Right;
		QPushButton *mBtn2Left;

		QPushButton *mBtnOk;
		QPushButton *pushApply;
		QPushButton *pushCancel;

		int mPreSelectId{ -1 };

		DishList mLeftList;
		DishList mRightList;
	};
}

namespace N_ORDERSYS
{

	/**
	*	@brief 点菜界面
	*
	*	this class
	*/
	class OrderDishDlg :public OrderDlg
	{
	public:
		OrderDishDlg(DishList::ptr lst, DishList::ptr rst);
		OrderDishDlg(const DishList& lst, const DishList& rst);
		virtual ~OrderDishDlg();

		DishList getSupportDishList() { return getLeftDishList(); }
		DishList getOrderDishList() { return getRightDishList(); }
	};
}

namespace N_ORDERSYS
{

	/**
	*	@brief 上菜界面
	*
	*	this class
	*/
	class UpDishDlg :public OrderDlg
	{
	public:
		UpDishDlg(DishList::ptr lst, DishList::ptr rst);
		UpDishDlg(const DishList& lst, const DishList& rst);
		virtual ~UpDishDlg();

		//DishList getOrderDishList() { return getLeftDishList(); }
		DishList getHadupDishList() { return getRightDishList(); }
	};
}