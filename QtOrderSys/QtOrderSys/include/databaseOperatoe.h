﻿/**
*	@file 数据库的操作类，此类中组装SQL语句
*
*
*	@author yexian
*	@date
*/



#pragma once
#include "libpq-fe.h"
#include "AppContext.h"
#include "LoginUser.h"
#include <string>

namespace N_ORDERSYS {
	//数据库和表在外面创建，这里不创建数据库
	//数据库名：ORDERSYS

	/**
	*	@brief 数据库操作类，外部直接调用接口
	*
	*	其实还可以继续拆分成只负责数据库，不负责业务，暂时先这样处理
	*/
	class DataBaseOperator {
	public:

		///	@brief	单例函数，获取数据库连接对象
		static DataBaseOperator& instance() {
			static auto ret = DataBaseOperator();
			return ret;
		}
		~DataBaseOperator() = default;

		/// @brief 数据库初始化，调用一次
		bool initDataBase();
		/// @brief 获取数据库连接的指针指针对象，如果为空表示连接不成功
		std::shared_ptr<PGconn > getDBConnect();
	private:
		DataBaseOperator();
	private:
		std::string  mConnStr{ "dbname=ordersys" };		///<	数据库连接字符串
	};
}