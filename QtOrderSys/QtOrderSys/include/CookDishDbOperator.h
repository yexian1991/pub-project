﻿#pragma once
#include "libpq-fe.h"
#include "AppContext.h"
#include "CookDish.h"

namespace N_ORDERSYS {


	/**
	*	@brief 外部操作“菜单”的操作类
	*	CREATE TABLE "tCookDish" (dishid int PRIMARY KEY,dishname varchar(20) NOT NULL,
	*  dishtype int NOT NULL default 3,dishprice int NOT NULL default 20,dishcount int default 0);
	*	需要使用组装类对sql语句进行组装后自动生成，不要直接用PGconn进行操作
	*/
	class CookDishDbOperator {
	public:
		CookDishDbOperator(const std::shared_ptr<PGconn>& conn);
		///
		bool hasDish(int id);
		///新增加菜式
		bool addDish(const CookDish& dish);
		///删除菜式
		bool removeDish(int id);
		///修改菜式
		bool alterDish(const CookDish& dish);
		///设置某菜式剩余数量
		bool alterXDishCount(int id, int count);
		///获取某菜式剩余数量
		bool lookXDishCount(int id, int& count);
		///获取某菜式价格
		bool lookXDishPrice(int id, int& count);
		///查询所有菜式信息
		bool lookStaff(CookListMap & dishs);
		///查询指定菜式信息
		bool lookStaff(int id, CookDish& dish) ;
	private:
		std::string  mDishTableName{ "tCookDish" };			///<	数据表的名称：用户，需要事先创建
		std::shared_ptr<PGconn> mDBconnect{ nullptr };
	};
}