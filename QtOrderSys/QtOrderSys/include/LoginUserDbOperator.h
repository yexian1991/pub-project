﻿#pragma once
#include "libpq-fe.h"
#include "AppContext.h"
#include "LoginUser.h"
#include <string>

namespace N_ORDERSYS {

	/**
	*	@brief 外部操作“系统用户LoginUser”的操作类
	*
	*	CREATE TABLE "tAccount" (account varchar(20) PRIMARY KEY,name varchar(20) NOT NULL,pwd varchar(20) CHECK(CHAR_LENGTH(pwd)>8),
	*	phone varchar(20) NOT NULL,addr varchar(100),age int NOT NULL,sex int NOT NULL,userLmt int);
	*	需要使用组装类对sql语句进行组装后自动生成，不要直接用PGconn进行操作
	*/
	class LoginUserDbOperator {
	public:
		LoginUserDbOperator(const std::shared_ptr<PGconn>& conn);
		///判断账号对应用户是否存在
		bool hasUser(const std::string taccount);
		///判断密码是否正确
		bool isRightpwd(const std::string taccount, const std::string& pwd);
		///判断用户是否有权限
		bool hasLimit(const std::string taccount);
		///新增加用户
		bool addStaff(const LoginUser& usr);
		///修改用户
		bool alterStaff(const LoginUser& usr);
		///删除用户
		bool removeStaff(const LoginUser& usr);
		///删除用户
		bool removeStaff(const std::string & usrAccount);
		///查询所有用户信息
		bool lookStaff(std::map<std::string, LoginUser >& users)const;
		///查询指定用户信息
		bool lookStaff(const std::string & usrAccount, LoginUser& user) const;
	private:
		std::string  mUserTableName{ "tAccount" };			///<	数据表的名称：用户，需要事先创建
		std::shared_ptr<PGconn> mDBconnect{ nullptr };
	};
}