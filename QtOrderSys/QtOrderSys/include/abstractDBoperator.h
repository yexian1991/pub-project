#pragma once

#ifdef OPENABSDB



#include "libpq-fe.h"

#include <memory>
#include <string>
#include<tuple>
#include <vector>

namespace N_ORDERSYS {

	///数据库支持的数据类型，根据实际需求进行添加
	enum DBVALUETYPE {
		DBVT_BOOL,		///< BOOL类型的数据
		DBVT_INT,			///< INT类型的数据
		DBVT_REAL,			///< double类型的数据
		DBVT_CHAR20,	///< 最大长度是20的字符串
		DBVT_CHAR100	///< 最大长度是100的字符串
	};

	///数据库限制条件
	enum DBVALUELIMIT {
		DBVL_PRIMARYKEY,			///< 唯一主键
		DBVL_NOTNULL					///< 不能为空
	};


	/// @brief 每种操作数据库的具体类的接口
	///
	/// 使用这个接口的人应该是不了解SQL的，但是可以根据方法的要求提供合适的参数
	/// 本处不做此复杂的封装
	class DBSQLoperator {
	public:
		using ptr = std::shared_ptr<DBSQLoperator>;
		using ParamType = std::vector<std::tuple<std::string, DBVALUETYPE, DBVALUELIMIT> >;
		using DataType = std::vector< std::tuple<std::string, DBVALUETYPE, std::string> >;
		~DBSQLoperator() = default;

		/// @brief 获得一个表的连接
		virtual void getConnect(const std::string& tableName);
		/// @brief 新建表
		virtual void createTable(const ParamType & param);
		/// @brief 删除表
		virtual void dropTable(const std::string& tableName);
		/// @brief 插入数据
		virtual void insertData(const DataType& data);
		/// @brief 删除数据
		virtual void dropData();
		/// @brief 修改数据
		virtual void alterData();
		/// @brief 查询数据
		virtual void selectData();
		/// @brief 清空数据
		virtual void clearData();
	protected:
		std::string& mTableName;
		ParamType mParams;
	};
}

#endif // OPENABSDB