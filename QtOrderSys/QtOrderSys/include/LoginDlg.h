﻿#pragma once
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>


namespace N_ORDERSYS
{
	///@brief 登录结果代码
	enum class LoginErrorType {
		LE_USERSTRSPACE			///<用户输入空
		, LE_USERSTRERROR		///<用户字符串格式不符
		,LE_NOUSER						///<无此用户
		, LE_PWDSTRSPACE		///<密码为空
		, LE_PWDSTRERROR		///<密码格式不符
		, LE_ERRORPWD				///<密码错误
		,LE_SUCESS						///<用户密码都正确
	};

	///登录界面
	class LoginDlg :public QDialog
	{
	public:
		LoginDlg(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
		LoginDlg();
		~LoginDlg();
	private:
		void initFB();
		/// @brief 判断账号密码的正确性
		LoginErrorType judgeUserPwd(const std::string& name, const std::string& pwd);
		void accept()override;
	private:
		QGridLayout *gridLayout_2;
		QHBoxLayout *horizontalLayout_4;
		QWidget *widget;
		QVBoxLayout *verticalLayout;
		QSpacerItem *verticalSpacer;
		QHBoxLayout *horizontalLayout;
		QLabel *label;
		QLineEdit *mUserName;
		QHBoxLayout *horizontalLayout_2;
		QLabel *label_2;
		QLineEdit *mUserPassword;
		QHBoxLayout *horizontalLayout_3;
		QSpacerItem *horizontalSpacer;
		QPushButton * mBtnOk;
		QPushButton *mBtnCancel;
		QSpacerItem *verticalSpacer_2;
	};

}