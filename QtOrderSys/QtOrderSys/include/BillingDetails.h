﻿#pragma once
#include <qobject.h>
#include "DishList.h"

namespace N_ORDERSYS {

	/**
	*	@brief 记录一次服务中账单相关的数据
	*
	*	包括点的餐列表，上的餐列表和服务记录，服务记录是否合适在这里
	*/
	class BillingDetails :public QObject
	{
	public:
		using ptr = std::shared_ptr<BillingDetails>;
	public:
		BillingDetails();
		~BillingDetails();

		DishList::ptr getOrderDishList() const { return mOrderDishList; }
		void setOrderDishList(const DishList&  lst) { *mOrderDishList = lst; }

		DishList::ptr getHadUpDishList() const { return mHadUpDishList; }
		void setHadUpDishList(const DishList&  lst) { *mHadUpDishList = lst; }

		std::string getServerLog_u8() const { return mServerLog; }
		void setServerLog_u8(const std::string &log) { mServerLog = log; }
		void appendServerLog_u8(const std::string &log) { mServerLog += log; }

		bool to_DB();
		bool from_DB();
	private:
		DishList::ptr mOrderDishList{ nullptr };		///<点餐列表
		DishList::ptr mHadUpDishList{ nullptr };		///<上菜列表
		std::string mServerLog ;					///<服务记录，需要升级成vector< log >形式

		int mSumXiaofei;
		int id;
	};
}
