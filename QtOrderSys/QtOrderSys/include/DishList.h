﻿#pragma once
#include "CookDish.h"
#include <qobject.h>
#include <functional>

namespace N_ORDERSYS
{

	///订单中的已点菜单
	class DishList:public QObject
	{
		friend DishList operator-(const DishList& a, const DishList& b);
		friend DishList operator+(const DishList& a, const DishList& b);
	public:
		using ptr = std::shared_ptr<DishList>;
		DishList();
		DishList(const CookListMap& lst);
		DishList(const DishList& ths);
		DishList(const DishList&& ths);
		virtual ~DishList();

		DishList& operator=(const DishList& ths);
		DishList& operator=(const DishList&& ths);

		/// @brief 比较两个列表完全相同
		bool operator==(const DishList& ths);
		bool operator!=(const DishList& ths);

		bool isEmpty()const;
		bool hasDish(int id)const;
		bool hasDish(const CookDish & dish)const;
		CookDish getDish(int id)const ;
		CookDish getDish(const CookDish & dish)const;

		void addDish(const CookDish& dish , int count=1);
		bool removeDish(const CookDish& dish, int count = 1);
		bool removeDish(const std::string& dishname, int count = 1);

		CookListMap getOrderList() const { return mDishList; }
		CookList getTypeOrderList(DISHTYPE type) { return mDishList[type]; }
		std::string getOrderListStr_u8();

		void for_each(std::function<void (CookDish& dish)> fun)const ;
	protected:
		CookListMap mDishList;
	};
	//列表相减，左没有右有的提示错误日志，不做处理，还是抛出异常？
	DishList operator-(const DishList& a, const DishList& b);
	DishList operator+(const DishList& a, const DishList& b);

}

namespace N_ORDERSYS
{
	/// @brief 菜单对象，有多种菜单可以切换
	///
	/// 初始化的时候从数据库读取数据，形成菜单
	/// 可以手动修改菜式品类，包括增加减少
	/// 可以手动修改菜式库存，TODO扩展自动修改，一键修改
	///	菜单保存之后写回数据库
	class SupportDishList :public DishList
	{
	public:
		using ptr = std::shared_ptr<SupportDishList>;
		///@brief 单例引用获取
		static SupportDishList& instance() {
			static SupportDishList lst;
			return lst;
		}

		/// @brief 初始化单例菜单，程序运行中获取上次的设定的对象
		bool init();
		DishList::ptr getSupportDishListCopy();
		void setSupportDishList(const DishList& p);
		//DishList getLstSupportDishList();

		~SupportDishList();
	private:
		SupportDishList() = default;
	};
}