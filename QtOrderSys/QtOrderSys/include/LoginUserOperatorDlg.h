﻿#pragma once
#include <qobject.h>
#include "qdialog.h"
#include "QTableWidget"
#include "QPushButton"
#include "QSpacerItem"
#include "QHBoxLayout"

#include "LoginUser.h"

#include <string>
#include <map>


namespace N_ORDERSYS {

	/**
	*	@brief 账户操作对话框的操作层，主要负责和数据库进行交互
	*/
	class LoginUserOperator {
	public:
		///LoginUserOperator的智能指针
		using ptr = std::shared_ptr<LoginUserOperator>;
	public:
		///返回账户列表的标头信息
		std::map<decltype(PI_ACCOUNT), std::string > getItemlabel();
		/// @brief 查询数据库中所有账户的信息
		bool lookStaff(std::map<std::string, LoginUser >&);
		/// @brief 查询数据库中账户account的信息
		bool lookStaff(const std::string& account, LoginUser& user);
		///在数据库中增加账户信息，返回true成功
		bool addStaff(const LoginUser& user);
		///删除数据库中账户account的信息，返回true成功
		bool removeStaff(const std::string& account);
		///修改数据库中用户user的信息，返回true成功
		bool alterStaff(const LoginUser& user);
	};
}


namespace N_ORDERSYS {

	/**
	*	@brief 这是一个对登录账户进行管理的对话框
	*
	*
	*	通过本对话框，实现对登录账户的增删改查操作
	*/
	class LoginUserOperatorDlg :
		public QDialog
	{
	public:
		LoginUserOperatorDlg(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
		~LoginUserOperatorDlg();

	private:
		///初始化界面
		void initFB();
		///初始化展示账户的TableWidget控件的内容
		void initTable();
		///消息绑定
		void bindOperator();
		///是否选中表中的某行
		bool isSelectRow();
	private:
		LoginUserOperator::ptr mOperator{ nullptr };	///< 操作类
		QTableWidget *mTableWidget;	///< 展示系统中所有账户信息的表格控件

		QPushButton *mBtnReflush;		///< 刷新界面按钮
		QPushButton *mBtnCheck;			///< 查看某账户具体信息按钮
		QPushButton *mBtnAdd;			///< 添加账户按钮
		QPushButton *mBtnAlter;			///< 修改某账户按钮
		QPushButton *mBtnRemove;		///< 删除账户按钮
		QPushButton *mBtnOk;				///< 以确定方式退出对话框
		QPushButton *mBtnCancel;		///< 以取消方式退出对话框

	};

}