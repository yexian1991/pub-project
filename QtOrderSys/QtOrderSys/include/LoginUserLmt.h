﻿#pragma once
#include <bitset>
#include <map>
#include <string>

namespace N_ORDERSYS
{

	/**
	*	@brief 账户权限解析用的方法
	*
	*	从数据库中取出一个值，使用hasLmt解析出具有的权限
	*	设置一个账户具有的权限，生成一个数字用于存储在数据库
	*/
	class LoginUserLmt
	{
	public:
		LoginUserLmt();
		~LoginUserLmt();
		/// @brief 通过一个值初始化权限
		LoginUserLmt(int value);

		/// @brief 判断是否具有某项权限
		bool hasLmt(int code);
		/// @brief 设置是否具有某项权限
		void setLmt(int code, bool has = true);
		/// @brief 获取权限值
		int getValue();
	private:
		std::bitset<32> mLmt;		///< 合计支持32项分权限，每位表示一项权限

		static std::map<int, std::string > sLmtMap;		///< 保存权限的静态map，key是权限项，value是权限名
		static void initLmtMap();									///< 用于初始化权限的函数
	};
}
