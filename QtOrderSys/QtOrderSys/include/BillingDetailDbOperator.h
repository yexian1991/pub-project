﻿#pragma once
#include "libpq-fe.h"
#include "AppContext.h"
#include "BillingDetails.h"
#include <string>

namespace N_ORDERSYS {

	/**
	*	@brief 外部操作“账单”的操作类
	*
	*	CREATE TABLE "tBillingDetal" (dishid int PRIMARY KEY,dishname varchar(20) NOT NULL,
	*  dishtype int NOT NULL default 3,dishprice int NOT NULL default 20,dishcount int default 0);
	*	需要使用组装类对sql语句进行组装后自动生成，不要直接用PGconn进行操作
	*/
	class BillingDetailDbOperator {
	public:

		//时间，桌号，客户，菜单，金额
		BillingDetailDbOperator(const std::shared_ptr<PGconn>& conn);
		///判断账单是否存在

		///新增账单

		///修改账单

		///查找账单

		///删除账单

		///获得某项账单

		///获得全部账单

		///获得查询账单
	private:
		std::string  mBillTableName{ "tBillingDetal" };			///<	数据表的名称：账单，需要事先创建
		std::shared_ptr<PGconn> mDBconnect{ nullptr };
	};
}