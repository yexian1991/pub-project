﻿#pragma once
#include <memory>
#include <string>

namespace N_ORDERSYS {
	class CustomInfo {
	public:
		using ptr = std::shared_ptr<CustomInfo>;
		CustomInfo() = default;
		virtual ~CustomInfo() = default;//可能还有团购用户等子类

		std::string getPlayStr()const;
	private:

		//用于事后数据分析
		int personCount;
		int female;
		int male;
		int child;
		int old;
	};
}