#pragma once
#include <qwidget.h>
#include "qlistwidget.h"
#include "TableManager.h"

namespace N_ORDERSYS {
	class TableListWidget :	public QWidget
	{
	public:
		explicit TableListWidget(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
		~TableListWidget();

		virtual void resizeEvent(QResizeEvent *event)override;

	private:
		QListWidget* mTableList;

		std::vector<TableManager::ptr> mTableManagers;
	};
}

