﻿#pragma once
#include "yaml-cpp/yaml.h"
#include "log.h"

namespace N_ORDERSYS
{
	/// @brief 整个程序的启动配置管理类
	///
	///	启动信息管理类，内部存储程序运行不能少的信息
	/// 比如其他数据库的地址信息
	/// 支持的数据等
	/// 各项都以字符串的形式保存，使用者自己知道类型
	class AppContext {
	public:
		/// @brief 程序启动的最开始调用，获得程序相关配置信息
		static AppContext& instance() {
			static AppContext app;
			return app;
		}
		/// @brief 初始化此对象，只需要调用一次
		void init();
		/// @brief 初始化此对象，只需要调用一次
		void initAfterDb();
		/// @brief 如果修改context内容之后通过此接口保存至文件
		bool save();

		template<typename T>
		bool query(const std::string & key, T& data) {
			if (mContext.IsNull()) {
				OLOG_E() <<"CANT QUERY FROM CONTEXT ,CONTEXT IS NULL";
				return false;
			}
			try {
				data = mContext[key].as<T>();
			}
			catch (...) {
				OLOG_E() <<"CANT QUERY FROM CONTEXT ,VALUE IS NOT EXIST";
				return false;
			}
			OLOG_D() << "QUERY FROM CONTEXT" << key<< data;
			return true;
		}
	private:
		/// @brief 从文件读取数据点熬内存中
		bool load();
		AppContext() = default;

		YAML::Node mContext;		///<启动配置，全部转字符串进行存储
		const std::string mConfigFilePath{ "OrderSys.yaml" };		///< 本配置文件的路径
		//const std::string mConfigFilePath{ "D:/4cplusplus/pub-project/QtOrderSys/QtOrderSys/bin/OrderSys.yaml" };
	};
}