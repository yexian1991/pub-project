﻿#pragma once
#include <qobject.h>
#include <string>


namespace N_ORDERSYS {

	/**
	*	@brief 表示用户信息项的索引
	*
	*	顺序和数据库中的顺序一致，由于数据库中的数据都可以用字符串表示
	*	为了能自由转成string、int、bool等格式，通过本enum作为索引存储在LoginUser对象中
	*/
	enum PARAM_INDEX{
		PI_ACCOUNT,	///< 账户
		PI_NAME,			///< 姓名
		PI_PWD,				///< 密码
		PI_PHONE,		///< 电话号码
		PI_ADDER,		///< 联系地址
		PI_AGE,				///< 年龄
		PI_SEX,				 ///< 性别
		PI_LMT,				 ///< 账户权限
		PL_MAX				///< 索引的最大值
	};
}


namespace N_ORDERSYS
{

	/**
	*	@brief	保存有一个用户信息的类
	*
	*	一个账户的信息保存在此类，提供了简单的获取用户信息的接口
	*	提供了对象转数据库字符串和字符串转对象的接口
	*/
	class LoginUser :public QObject
	{
	public:
		LoginUser();
		LoginUser(const std::string& account,const std::string& name
			, const std::string& pwd, const std::string& phone
			, int age,int sex=1, const std::string& addr=""
			, int lmt=-1);
		~LoginUser();

		LoginUser& operator=(const 	LoginUser&);
		LoginUser(const LoginUser&);

		std::string getAccount()const {	return mAccount;	}
		std::string getName()const { return mName; }
		std::string getPassword()const { return mPassword; }
		std::string getPhone()const { return mPhone; }
		std::string getAdder()const { return mAdder; }
		int getAge()const { return mAge; }
		int getLmt()const { return mLmt; }
		bool isFemale() const { return mSex == 0;	}

		/// @brief 判断当前对象的内容是否满足预定的要求 
		std::pair<bool,std::string> isPerfect()const ;

		/// @brief 判断两个对象是否一致 
		bool operator==(const LoginUser& usr);

		/// @brief 对象转成数据库中需要的字符串的格式 
		std::string toDBstr()const;
		/// @brief 数据库中第index列的值转成对象中对应项的值
		bool  fromDBstr(decltype(PARAM_INDEX::PL_MAX) index,const std::string& value);
	private:
		std::string mName{};				///< 账户
		std::string mAccount{};			 ///< 姓名
		std::string mPassword{};			///< 密码
		int			mSex{ 0 };				///< 电话号码
		int			mAge{};					 ///< 联系地址
		std::string mPhone{};				 ///< 年龄
		std::string mAdder{};				 ///< 性别
		int			mLmt{};					 ///< 账户权限
	};

}