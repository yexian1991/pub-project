﻿#pragma once
#include "LoginUser.h"
#include <qdialog.h>
#include "qvariant.h"
#include "qcombobox.h"
#include "qgroupbox.h"
#include "qlabel.h"
#include "qpushbutton.h"
#include "qradiobutton.h"
#include "qspinbox.h"
#include "qlineedit.h"
#include "qcheckbox.h"


namespace N_ORDERSYS
{
	///展示某一个账户的详细页面基类
	class LoginUserAlterDlg :	public QDialog
	{
	public:
		///默认构造
		LoginUserAlterDlg();
		///通过账户对象构造
		LoginUserAlterDlg(const LoginUser & user);
		virtual ~LoginUserAlterDlg() = default;
		///从对话框对象获得一个LoginUser对象
		LoginUser getValue() const;
		///响应确定按钮事件，在其中将会判断数据内容是否合适
		void accept()override;
	protected:
		///子类构造函数中调用
		void create();
		///对话框控件状态设置，对话框标题设置
		virtual void initDlgState() = 0;
		///控件消息绑定
		virtual void bindOperator();
		///输入控件设置过滤条件
		virtual void setCtrlValidator();
	private:
		///初始化窗口控件
		void initFB();
		void initValue();
	protected:
		LoginUser mOldUser;

		QLineEdit *mEdtPwd;
		QLineEdit *mEdtAddr;
		QLineEdit *mEdtPhone;
		QLineEdit *mEdtAccount;
		QLineEdit *mEdtName;
		QSpinBox *mSBAge;
		QRadioButton *mRBfemale;
		QRadioButton *mRBmale;

		QCheckBox *mCBdinner;
		QCheckBox *mCBbill;
		QCheckBox *mCBfood;
		QCheckBox *mCBhr;

		QPushButton *mBtnOk;
		QPushButton *mBtnCancel;
	};
}


namespace N_ORDERSYS
{
	///查看用户信息对话框
	class LoginUserAlterDlgCheck : public LoginUserAlterDlg
	{
	public:
		LoginUserAlterDlgCheck(const LoginUser & user);

	protected:
		void initDlgState() override;
	private:
		void disableCtrl();
	};
}

namespace N_ORDERSYS
{
	///增加新用户信息对话框
	class LoginUserAlterDlgAdd : public LoginUserAlterDlg
	{
	public:
		LoginUserAlterDlgAdd();
	protected:
		void initDlgState() override;
	};
}

namespace N_ORDERSYS
{
	///修改已有用户信息对话框
	class LoginUserAlterDlgAlter : public LoginUserAlterDlg
	{
	public:
		LoginUserAlterDlgAlter(const LoginUser & user);
	protected:
		void initDlgState() override;
		virtual void bindOperator() override;
	private:
		void disableCtrl();
	};
}