#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtOrderSys.h"
#include "qmenubar.h"
#include "qmenu.h"

namespace N_ORDERSYS {
	class QtOrderSys : public QMainWindow
	{
		Q_OBJECT

	public:
		QtOrderSys(QWidget *parent = Q_NULLPTR);
	private:
		void initFB();
		void initAction();
		void initMenu();
		void initToolBar();
		void initStatusBar();
		void initBind();
	private:
		QMenuBar *menuBar;
		QAction *	mAcTable;
		//QAction *	mAcOpenTable;
		//QAction *	mAcDinner;
		//QAction *	mAcOperator;
		//QAction *	mAcSettle;
		QAction *	mAcAddFoodStyle;
		QAction *	mAcRemoveFoodStyle;
		QAction *	mAcAlterFoodStyle;
		QAction *	mAcAlterFoodCount;
		QAction *	mAcDailyBill;
		QAction *	mAcMonthlyBill;
		QAction *	mAcAnnualBill;
		QAction *	mAcLookStaff;
		QAction *	mAcAddStaff;
		QAction *	mAcRemoveStaff;
		QAction *	mAcAlterStaff;
		QAction *	mAcHelp;
		QAction *	mAcAbout;

		QWidget* mCenterWidget;

		Ui::QtOrderSysClass ui;
	};
}