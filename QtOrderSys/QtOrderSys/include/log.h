﻿/*
  * Copyright (C): wendy
  * FileName: 	log.h
  * Description:
  * 2021.7.14修复少量log没有保存的问题
  */

#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
//#include <filesystem>
#include <mutex>
#include <thread>
#include <future>
#include <atomic>
#include <condition_variable>
#include <memory>

//一种是格式化%d输出
//示例：LOG_D("x=%d,y=%s",x,y);
#define LOG_D Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).debug
#define LOG_I   Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).info
#define LOG_W Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).warning
#define LOG_E  Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).error
#define LOG_F  Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).fatal

//一种流方式输出
//示例：OLOG_D() << 10 << "this is ostreamFmt ";
#define OLOG_D Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).odebug
#define OLOG_I  Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).oinfo
#define OLOG_W Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).owarning
#define OLOG_E Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).oerror
#define OLOG_F Nwendy::Logger(__FILE__,__FUNCTION__,__LINE__).ofatal

//指定参数的全局初始化宏，过滤等级，写入器，buffer大小
//示例：NWENDY_LOG_INIT(Nwendy::LogLevel::LL_DEBUG, std::make_shared<Nwendy::LogStdAppend>(), 100);
#define NWENDY_LOG_INIT(level,appendPtr,bufferSzie,interval_s)\
	Nwendy::LogManager::getInstance().init();\
	Nwendy::LogManager::getInstance().setLevel(level);\
	Nwendy::LogManager::getInstance().addAppend(appendPtr);\
	Nwendy::LogManager::getInstance().setBufferSize(bufferSzie);\
	Nwendy::LogManager::getInstance().setTimerSwapInterval(interval_s);
	

#define NWENDY_FILEAPPEND(fileDir)\
	std::make_shared<Nwendy::LogFileAppend>(fileDir)

#define NWENDY_STDAPPEND()\
	std::make_shared<Nwendy::LogStdAppend>()

namespace Nwendy {
	//日志等级
	class LogLevel {
	public:
		enum Level {
			LL_ALL = 0,
			LL_DEBUG = 1,
			LL_INFO = 2,
			LL_WARN = 3,
			LL_ERROR = 4,
			LL_FATAL = 5,
			LL_NONE
		};

		static std::string toString(LogLevel::Level level);
		static LogLevel::Level fromString(const std::string& str);
	};
}

namespace Nwendy {
	//日志包含的内容，通过全局可以设置显示内容
	//Nwendy::LogManager::getInstance().setDisplayFmt(static_cast<int>(Nwendy::MessageParams::MP_ALL)&(~static_cast<int>(Nwendy::MessageParams::MP_FILENAME)));
	enum class MessageParams
	{
		MP_LEVEL = 0x01 << 0,
		MP_FILENAME=0x01<<1,
		MP_FUNCNAME = 0x01<<2,
		MP_LINE = 0x01 << 3,
		MP_THREADID = 0x01 << 4,
		MP_FIBERID = 0x01 << 5,
		MP_TIME = 0x01 << 6,
		MP_MESSAGE = 0x01 << 7,
		MP_ALL= MP_LEVEL| MP_FILENAME| MP_FUNCNAME | MP_LINE | MP_THREADID | MP_TIME | MP_MESSAGE
	};
	static int mParamFlags = static_cast<int>(Nwendy::MessageParams::MP_ALL);
}


namespace Nwendy {
	//一条日志的内容
	class LogEvent {
	public:
		using ptr = std::shared_ptr<LogEvent>;

		LogEvent(const std::string& fileName, const std::string& functionName, int line);

		void setLevel(Nwendy::LogLevel::Level lvl) { mLevel = lvl; }
		int getLevel() { return static_cast<int>(mLevel); }

		//格式化输入message
		void setMessage(const std::string & msg) { mMessage =std::move(msg); }
		//流输入message
		std::stringstream&	getSS() { return mSS; }
		//转为字符串格式，方便以字符串存储的方法的调用
		std::string toString(LogEvent::ptr	ptr);
	private:
		Nwendy::LogLevel::Level mLevel;
		std::string mFileName;
		std::string mFunctionName;
		std::string mMessage;
		int mLineNo;
		int mProcessId;//进程id，暂时不用
		std::thread::id mThreadId;
		time_t mTime;

		std::stringstream mSS;
	};
	//时间转字符串，如果要自定义格式可以在此
	static std::string getTimeStr(time_t t,const std::string& fmt= "%Y-%m-%d %H:%M:%S");
}


namespace Nwendy {
	//日志器对象，每一条日志直接就是一个Logger的临时对象
	class Logger {
	public:
		Logger(const char* fileName, const char* functionName, int line);

		//析构的时候将日志输出到buffer中
		~Logger() { log(mLogEvent); }
		void log(const LogEvent::ptr & event);

		std::stringstream& odebug();
		std::stringstream& oinfo();
		std::stringstream& owarning();
		std::stringstream& oerror();
		std::stringstream& ofatal();

		void debug(const char* fmt, ...);
		void info(const char* fmt, ...);
		void warning(const char* fmt, ...);
		void error(const char* fmt, ...);
		void fatal(const char* fmt, ...);

		static void setMinLevel(LogLevel::Level lvl) { mMinLogLevel = lvl; }
	private:
		//内部有一个event指针
		Nwendy::LogEvent::ptr mLogEvent;
		//筛选日志等级
		static Nwendy::LogLevel::Level mMinLogLevel;
	};
}

namespace Nwendy {
	//输出目的的基类
	class LogAppend {
	public:
		using ptr = std::shared_ptr<Nwendy::LogAppend>;
		virtual void display(const std::vector<LogEvent::ptr>& vct) = 0;
	};

	//标准输出
	class LogStdAppend :public LogAppend {
	public:
		using ptr = std::shared_ptr<Nwendy::LogStdAppend>;
		virtual void display(const std::vector<LogEvent::ptr>& vct) override;
	};

	//文件输出
	class LogFileAppend :public LogAppend {
	public:
		using ptr = std::shared_ptr<Nwendy::LogFileAppend>;
		LogFileAppend(const std::string & fileDir);
		virtual void display(const std::vector<LogEvent::ptr>& vct) override;
	private:
		std::fstream open();
		std::string  getFileName();
		//需要文件大小限制或者按天限制，在open里处理，调用getFileName，更新mCurFilePath
	private:
		std::string mFileDir;
		//std::filesystem::path mCurFilePath;
		std::string mCurFilePath;
	};

	//...
}

namespace Nwendy {
	//缓存队列，一定时间或者一定数量保存
	//开两个缓存，一个保存一个接收，如果都没有空则丢弃日志
	class LogBufferManager {
	public:
		using ptr = std::shared_ptr<LogBufferManager>;

		LogBufferManager(size_t bufferSize);
		~LogBufferManager();
		void setBufferSize(size_t size);
		void setTimerSwapInterval(int interval_s);
		//将数据push到buffer_1
		bool addLogEvent(const LogEvent::ptr& event);
		//将mBuffer_2的数据写入到append
		void log2Append();
		//添加移除终断
		void addAppend(LogAppend::ptr ptr);
		void removeAppend(LogAppend::ptr ptr);
		//时间到进行交换
		void timerSwap();
		//停止线程
		void stopLogSaveThread(bool stop = true) { mStop.store(stop); }
	private:
		//如果buffer_1满或者等待时间到,和buffer_2进行数据交换
		void swapBuffer();
	private:
		std::vector<LogEvent::ptr> mBuffer_1;
		std::vector<LogEvent::ptr> mBuffer_2;
		std::vector<LogAppend::ptr> mLogAppends;
		size_t mMaxBufferSize{ 100 };
		std::atomic<int> mTimerSwapInterval_s{ 5 };

		std::mutex mBufferMutex;//buffer2写时候不能swap
		std::condition_variable mConditionVariable;
		std::atomic<bool> mStop{ false };

		std::mutex mPushLogMutex;//多线程往buffer1写
		std::mutex mLogAppendMutex;//修改append时候保护
	};
}

namespace Nwendy {
	//全局的日志管理类，用于初始化和中途设置参数
	//可以修改为从文件读取配置进行设置
	class LogManager {
	public:
		static LogManager& getInstance();
		void init();

		LogBufferManager::ptr getBuffer() { return mLogBufferMag; }
		//中途设置参数
		void setLevel(LogLevel::Level lvl);
		void setDisplayFmt(int flags=static_cast<int>(Nwendy::MessageParams::MP_ALL));
		void setBufferSize(int size);
		void setTimerSwapInterval(int interval_s);
		void addAppend(LogAppend::ptr ptr){ mLogBufferMag->addAppend(ptr); }
		void removeAppend(LogAppend::ptr ptr) { mLogBufferMag->removeAppend(ptr); }
	private:
		LogManager() = default;
		~LogManager();

		//LogBufferMag指针
		LogBufferManager::ptr mLogBufferMag;
		//保存数据线程
		std::thread mSaveBufferThread;
		//定时交换数据线程
		std::thread mTimerSwapThread;
	};
}
