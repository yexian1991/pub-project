﻿#pragma once
#include <qobject.h>

namespace N_ORDERSYS
{
	///菜品的分类，暂定为凉菜、主菜、饮料，其他
	enum DISHTYPE {
		DT_SIDEDISH
		,DT_MAINDISH
		,DT_DRINKS
		,DT_OTHERS
	};
}

namespace N_ORDERSYS
{

	/**
	*	@brief 单个菜品类
	*
	*	简单菜品包括id、名称、价格、分类；数量count有不同的定义，可以是库存，也可以是已点也可以是已上
	*/
	class CookDish :public QObject
	{
	public:
		static  DISHTYPE getCookType(int id);
		static  DISHTYPE getCookType(const CookDish&  dish);

		CookDish();
		CookDish(int id);
		~CookDish();

		CookDish(const CookDish& ths);
		CookDish& operator=(const CookDish& ths);
		/// @brief 判断两个对象id是否一致
		bool isSameId(const CookDish& ths) const;
		/// @brief 判断两个对象是否完全一致
		bool operator==(const CookDish& ths) const ;
		bool operator!=(const CookDish& ths) const;
		/// @brief 用于对菜品的排序，以id为key
		bool operator>(const CookDish& ths) const;
		bool operator<(const CookDish& ths) const;
		/// @brief 对象转成数据库中需要的字符串的格式 
		std::string toDBstr()const;
		/// @brief 数据库中第index列的值转成对象中对应项的值
		bool  fromDBstr(int index, const std::string& value);

		int getId()const { return mId; }
		int getPrice()const { return mDishPrice; }
		int getCount()const { return mDishCount; }
		std::string getDishName()const { return mDishName; }
		DISHTYPE getType()const { return mDishType; }


		void setCount(int count=1) { mDishCount = count; }
		void incCount(int count = 1) { mDishCount += count; }
		void decCount(int count = 1) { mDishCount -= count; }
		void setDishName(const std::string& name) { mDishName = name; }
		void setDishPrice(int price) { mDishPrice = price; }

	protected:
		int mId;									///<菜式对应的ID，type*100+id
		std::string mDishName;			///<菜式名称
		int mDishPrice;						///<菜式价格
		DISHTYPE mDishType;			///<菜式所属类型
		int mDishCount;						///<菜式数量(库存)
	};

	///菜单列表
	using CookList = std::vector<CookDish>;
	///分类的菜单列表
	using CookListMap = std::map<DISHTYPE, CookList>;
}
