﻿#pragma once
#include <memory>
#include <string>
#include <map>
#include <tuple>
#include <chrono>

namespace N_ORDERSYS {
	///
	enum  EN_TableState {
		TS_OK,						//可以正常开台的餐桌
		TS_ORDERING,		//客户点菜中
		TS_COOKING,			//后厨准备中
		TS_UPING,					//上菜中
		TS_SERVING,			//享用中
		TS_CLEANING			//清理中
	};

	struct TableStateCell {
		EN_TableState mState{ EN_TableState::TS_OK };
		int mColor{ 0 };
		std::string mName{ "" };
	};

	class TableStateMap{
	public:
		static TableStateMap& instance() {
			static TableStateMap mp;
			return mp;
		}
		void init();
		TableStateCell get(EN_TableState) const;
		int getTableColor(EN_TableState) const;
	private:
		bool fromFileCfg();//颜色永固可以配置
		void defaultCfg();
	private:
		std::map<EN_TableState, TableStateCell> mData;
	};
}

namespace N_ORDERSYS {
	class TableStateContext;
}

namespace N_ORDERSYS {
	class TableState
	{
	public:
		using ptr = std::shared_ptr<TableState>;
		TableState();
		virtual ~TableState();

		virtual void Handle(TableStateContext* pContext) = 0;

		virtual EN_TableState getStateNow() const = 0;
		virtual std::string getStateNowStr() const = 0;
	};
}

namespace N_ORDERSYS {

	/**
	*	@brief 记录当前状态的信息和一些上下文信息
	*
	*	this class
	*/
	class TableStateContext {
	public:
		using ptr = std::shared_ptr<TableStateContext>;

		TableStateContext(TableState::ptr pState);
		~TableStateContext() = default;

		///当前是否可以提供服务
		bool isReady();
		///进入下一个状态，提供给外部使用者调用
		void next();

		std::map<std::chrono::system_clock::time_point, TableState::ptr> getHisTableState()const;
		///改变当前状态，通常是各个TableState的子类调用，外部不应该调用
		void changeState(TableState::ptr pState);
		///获得当前的状态
		EN_TableState getStateNow() const;
		///获得表示当前值状态的字符串
		std::string getStateNowStr() const;
	private:
		TableState::ptr mStatePtr{ nullptr };		///<当前状态

		std::map<std::chrono::system_clock::time_point,TableState::ptr> mHisTableState;		///<历史状态记录
	};

}

namespace N_ORDERSYS {
	class TableState_ok :public TableState {
	public:
		virtual void Handle(TableStateContext* pContext)override;
		virtual EN_TableState getStateNow() const override;
		virtual std::string getStateNowStr() const override;
	};
}

namespace N_ORDERSYS {
	class TableState_ordering :public TableState {
	public:
		virtual void Handle(TableStateContext* pContext) override;
		virtual EN_TableState getStateNow() const override;
		virtual std::string getStateNowStr() const override;
	};
}

namespace N_ORDERSYS {
	class TableState_cooking :public TableState {
	public:
		virtual void Handle(TableStateContext* pContext) override;
		virtual EN_TableState getStateNow() const override;
		virtual std::string getStateNowStr() const override;
	};
}

namespace N_ORDERSYS {
	class TableState_uping :public TableState {
	public:
		virtual void Handle(TableStateContext* pContext) override;
		virtual EN_TableState getStateNow() const override;
		virtual std::string getStateNowStr() const override;
	};
}

namespace N_ORDERSYS {
	class TableState_serving :public TableState {
	public:
		virtual void Handle(TableStateContext* pContext) override;
		virtual EN_TableState getStateNow() const override;
		virtual std::string getStateNowStr() const override;
	};
}

namespace N_ORDERSYS {
	class TableState_cleaning :public TableState {
	public:
		virtual void Handle(TableStateContext* pContext) override;
		virtual EN_TableState getStateNow() const override;
		virtual std::string getStateNowStr() const override;
	};
}