﻿#pragma once
#include "TableManager.h"
#include <qdialog.h>
#include "qpushbutton.h"
#include "qtextbrowser.h"
#include "qlabel.h"


namespace N_ORDERSYS
{

	/**
	*	@brief 一次服务对应的界面
	*
	*	此界面显示一次服务的状态，提供对一次服务整个过程的控制，记录一次服务过程的所有记录
	*/
	class TableServerDlg :public QDialog
	{
	public:
		using ptr = std::shared_ptr<TableServerDlg>;

		TableServerDlg(TableManager::ptr);
		~TableServerDlg();

	private:
		void initFB();
		void initValue();
		///刷新界面，在此会刷新按钮、状态ICON和桌台CION
		void reflushDlg();
		///刷新按钮
		void reflushBtn();
		///刷新桌台ICON
		void reflushTableIcon();
		///刷新状态ICON
		void reflushStateIcon();
		///刷新操作记录界面
		void reflushServerLogCtrl();

		void append2ServerLog(const std::string & info);
		///消息响应事件开台点菜
		void onBtnOpenTable();
		///消息响应事件点菜结束
		void onBtnOrderOver();
		///消息响应事件加减菜
		void onBtnAdd_Remove();
		///消息响应事件上菜
		void onBtnUpCook();
		///消息响应事件上菜结束
		void onBtnOverUp();
		///消息响应事件结账
		void onBtnSettle();
		///消息响应事件清理桌面
		void onBtnClean();
	private:
		///一次服务管理对象
		TableManager::ptr mTableManagerPtr;

		//用于展示当前状态的icon
		QLabel* mLabelTS_OK;
		QLabel* mLabelTS_ORDERING;
		QLabel* mLabelTS_COOKING;
		QLabel* mLabelTS_UPING;
		QLabel* mLabelTS_SERVING;
		QLabel* mLabelTS_CLEANING;

		QLabel *mTableIcon;			///<记录当前餐组服务状态的ICON
		QLabel* mCustomInfo;		///<记录客户信息
		QTextBrowser *mServerLog;		///<记录服务过程的消息

		QPushButton *mBtnOpenTable;			///<开台点菜
		QPushButton *mBtnOrderOver;			///<点菜完毕
		QPushButton *mBtnBeginUp;				///<开始上菜
		QPushButton *mBtnOverUp;				///<结束上菜
		QPushButton *mBtnSettle;					///<结账
		QPushButton *mBtnClean;					///<清理
		QPushButton *mBtnAdd_Remove;		///<加减菜
		QPushButton *mBtnExit;					///<退出
	};
}