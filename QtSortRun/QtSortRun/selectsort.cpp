#include "SelectSort.h"


SelectSort::SelectSort(SortDatas& data) :SortBase(data)
{
}

SelectSort::~SelectSort()
{

}

//void SelectSort::begin() {
//	mThread = std::move(std::thread(&SelectSort::buddle, this));
//}

void SelectSort::buddle() {
	DO_THREAD_STOP();
	for (int i = 0; i < mData.getData().size() - 1; i++) {
		int min = i;
		for (int j = i + 1; j < mData.getData().size(); j++) {
			DO_STOP();
			DO_EMIT(this->compareSort(), mIntervalSelected, mData.setCompareIndexs(j, min));
			if (mData.getData()[j] < mData.getData()[min]) {
				min = j;
			}
		}
		if (i != min) {
			DO_EMIT(this->swapSort(), mIntervalSwaped, mData.setSwapIndexs(i, min));
			//std::swap(mData.getData()[i], mData.getData()[min]);//����
			mData.swap(i, min);
			DO_EMIT(this->swapSort(), mIntervalSwaped, );
		}
	}

	DO_EMIT(this->compareSort(), mIntervalSelected, this->mData.clearCompareSelect());
}
