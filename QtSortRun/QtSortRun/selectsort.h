#ifndef SELECTSORT_H
#define SELECTSORT_H
#include "SortBase.h"

class SelectSort: public SortBase
{
    Q_OBJECT
public:
	SelectSort(SortDatas& data);
	~SelectSort();
    //virtual void begin()override;

protected:
	virtual void buddle()override;
};

#endif // SELECTSORT_H
