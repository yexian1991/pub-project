#include "quicksort3.h"


QuickSort3::QuickSort3(SortDatas& data) :SortBase(data)
{

}

QuickSort3::~QuickSort3()
{
}

//void QuickSort3::begin() {
//	mThread = std::move(std::thread(&QuickSort3::buddle, this));
//}

void QuickSort3::buddle() {
	DO_THREAD_STOP();
	__quickSort3(0, mData.getData().size()-1);
	DO_EMIT(this->compareSort(), mIntervalSelected, this->mData.clearCompareSelect());
}

std::pair<int, int> QuickSort3::__quickSortttt3(int left, int right) {
	srand(time(0));
	mData.swap(left, rand() % (right - left) + left);
	int value = mData.getData()[left];
	//参考值随机，将其移动到最左边

	//lt指向小于参考值之后的位置
	//gt指向大于参考值之前的位置
	//i是开始比较的那位
	int lt = left;
	int i = left + 1;
	int gt = right + 1;
	//结束循环时，左边的小于等于参考值，右边的大于参考值
	while (i < gt) {
		DO_STOP(std::make_pair(0,0));
		DO_EMIT(this->compareSort(), mIntervalSelected, mData.setCompareIndexs(i, left));
		//比较的值小于参考值，移动到小于最后一位
		if (mData.getData()[i] < value) {
			if (i != (lt + 1)) {
				DO_EMIT(this->swapSort(), mIntervalSwaped, mData.setSwapIndexs(lt + 1, i));
				//std::swap(arr[lt + 1], arr[i]);
				mData.swap(lt+1, i);
				DO_EMIT(this->swapSort(), mIntervalSwaped);
			}
			lt++;
			i++;
		}
		else if (mData.getData()[i] > value) {
			if (i != (gt - 1)) {
				DO_EMIT(this->swapSort(), mIntervalSwaped, mData.setSwapIndexs(i, gt - 1));
				//std::swap(arr[i], arr[gt - 1]);
				mData.swap(i, gt - 1);
				DO_EMIT(this->swapSort(), mIntervalSwaped);
			}
			gt--;//依旧指向gt-1是未处理的，所以不能i++
		}
		else// if (arr[i] == value)
			i++;
	}

	//循环结束，lt指向的值一定小于left，如果lt没有移动，那么left=lt
	if (left != lt) {
		DO_EMIT(this->swapSort(), mIntervalSwaped, mData.setSwapIndexs(left, lt));
		//std::swap(arr[left], arr[lt]);//最多是arr[left]和arr[left]交换
		mData.swap(left, lt);
		DO_EMIT(this->swapSort(), mIntervalSwaped);
	}
	return std::make_pair(lt, gt);
}

void QuickSort3::__quickSort3(int left, int right) {
	if (left >= right)
		return;
	auto mid = __quickSortttt3(left, right);
	DO_STOP();
	__quickSort3(left, mid.first - 1);
	__quickSort3(mid.second, right);
}
