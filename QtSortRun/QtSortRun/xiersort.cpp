#include "xiersort.h"


XierSort::XierSort(SortDatas& data) :SortBase(data)
{

}

XierSort::~XierSort()
{
}

//void XierSort::begin() {
//	mThread = std::move(std::thread(&XierSort::buddle, this));
//}

void XierSort::buddle() {
	DO_THREAD_STOP();
	auto length = mData.getData().size();
	for (int delta = length; delta>0; delta /= 2)
		for (int i = 0; i < delta; i++){
			DO_STOP();
			__xier(i, length, delta);
		}

	DO_EMIT(this->compareSort(), mIntervalSelected, this->mData.clearCompareSelect());
}

void XierSort::__xier(int st, int length, int delta) {
	for (int i = st + delta; i < length; i += delta) {
		for (int j = i; j > st; j -= delta){
			DO_STOP();
			DO_EMIT(this->compareSort(), mIntervalSelected, mData.setCompareIndexs(j, j - delta));
			if (mData.getData()[j] < mData.getData()[j - delta]){
				DO_EMIT(this->swapSort(), mIntervalSwaped, mData.setSwapIndexs(j, j - delta));
				//std::swap(arr[j], arr[j - delta]);
				mData.swap(j, j - delta);
				DO_EMIT(this->swapSort(), mIntervalSwaped);
			}
			else
				break;
		}
	}
}