#ifndef SORTDATAS_H
#define SORTDATAS_H
#include <vector>
#include <mutex>

static const auto NONE_SELECT{ std::make_pair<int,int>(-1, -1) };
class SortDatas{
public:

public:
    SortDatas(std::vector<int>& data){
        mDatas=data;
    }

    SortDatas(int size){
		srand(time(NULL));
        for(auto i=0;i<size;++i)
            mDatas.push_back(rand()%90+10);
    }

    //std::vector<int>& getData(){
    //    return mDatas;
    //}

	void swap(int indexA,int indexB){
		assert(indexA >= 0 && indexA < mDatas.size());
		assert(indexB >= 0 && indexB < mDatas.size());

		if (indexA == indexB)
			return;
		std::unique_lock<std::mutex>(mDataMtx);
		std::swap(mDatas[indexA], mDatas[indexB]);
	}

	std::vector<int> getData() const{
		std::unique_lock<std::mutex>(mDataMtx);
		return mDatas;
	}

    void setCompareIndexs(int indexa,int indexb){
		mCompareIndex=std::make_pair<>(indexa, indexb);
    }

	void setSwapIndexs(int indexa, int indexb) {
		mSwapIndex=std::make_pair<>(indexa, indexb);
	}

    void clearCompareSelect(){
		mCompareIndex = NONE_SELECT;
    }
	void clearSwapSelect() {
		mSwapIndex = NONE_SELECT;
	}

    auto getCompareIndex()const{return mCompareIndex;}
    auto getSwapIndex()const{return mSwapIndex;}
private:
    std::vector<int> mDatas;
	std::pair<int, int> mCompareIndex{ NONE_SELECT };
	std::pair<int, int> mSwapIndex{ NONE_SELECT };
  
	std::mutex mDataMtx;
};



#endif // SORTDATAS_H
