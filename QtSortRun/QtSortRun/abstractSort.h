#ifndef ABSTRACTSORT_H
#define ABSTRACTSORT_H
#include <QObject>
#include "SortDatas.h"
#include <QTimer>
#include <thread>


class abstractSort :public QObject{
    Q_OBJECT

public:
		enum SortType {
		ST_AUTO,
		ST_STEP
	};

public:
    virtual ~abstractSort()=default;
    virtual void begin()=0;
	virtual void oneStep() = 0;
	virtual void stopSort() = 0;
	virtual bool isStop() const = 0;

	virtual void setIntervalSelected(int mills) = 0;
	virtual void setIntervalSwaped(int mills) = 0;
	virtual int getIntervalSelected() const= 0;
	virtual int getIntervalSwaped() const = 0;
	virtual void setSortType(SortType) = 0;
	virtual SortType getSortType() const= 0;
Q_SIGNALS:
        void compareSort();
        void swapSort();
};




#endif // ABSTRACTSORT_H
