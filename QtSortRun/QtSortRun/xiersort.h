#pragma once
#include "SortBase.h"

class XierSort: public SortBase
{
    Q_OBJECT
public:
	XierSort(SortDatas& data);
	~XierSort();

   // virtual void begin()override;
protected:
	virtual void buddle() override;
private:
	void __xier( int length, int n, int k);
};
