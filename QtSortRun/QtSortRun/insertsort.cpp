#include "insertsort.h"


InsertSort::InsertSort(SortDatas& data) :SortBase(data)
{

}

InsertSort::~InsertSort()
{
}

//void InsertSort::begin() {
//	mThread = std::move(std::thread(&InsertSort::buddle, this));
//}

void InsertSort::buddle() {
	DO_THREAD_STOP();
	for (int i = 1; i < mData.getData().size(); i++) {
		for (int j = i; j > 0 && mData.getData()[j] < mData.getData()[j - 1]; j--) {
			DO_STOP();
			DO_EMIT(this->compareSort(), mIntervalSelected, mData.setCompareIndexs(j, j - 1));
			DO_EMIT(this->swapSort(), mIntervalSwaped, mData.setSwapIndexs(j, j - 1));
			//std::swap(mData.getData()[j], mData.getData()[j - 1]);
			mData.swap(j, j - 1);
			DO_EMIT(this->swapSort(), mIntervalSwaped,);
		}
	}
	DO_EMIT(this->compareSort(), mIntervalSelected, this->mData.clearCompareSelect());
}
