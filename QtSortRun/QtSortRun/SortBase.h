#pragma once
#include "abstractSort.h"
#include <atomic>
#include <condition_variable>
#include <QMetaMethod> 
#include <qevent.h>

#define DO_EMIT(signal,hold_ms,do_before_emit)\
do_before_emit;\
emit signal;	\
if(mSortType==ST_AUTO)\
	std::this_thread::sleep_for(std::chrono::milliseconds(hold_ms));\
else if(mSortType == ST_STEP){\
	std::unique_lock<std::mutex> tMutex(mStepMutex);\
	mStepCommend.wait(tMutex); \
}\
	

#define DO_STOP(xx)\
if (true == mIsStopThread.load()){\
return xx;\
}\

#define DO_THREAD_STOP()\
mIsStoped.store(false);\
mIsStopThread.store(false);\
auto x=std::shared_ptr<int>(new int,[this](int * i){delete i;mIsStoped.store(true);})\

class SortBase :public abstractSort {
public:
	SortBase(SortDatas& data):mData(data){}
	~SortBase() {
		mIsStopThread.store(true);
		if (mThread.joinable()) {
			mThread.join();
		}
	}

public:
	virtual void begin() {
		if (mData.getData().size() == 0)return;
		mData.clearCompareSelect();
		mData.clearSwapSelect();
		if (mThread.joinable()) {
			mThread.join();
		}
		mThread = std::move(std::thread(&SortBase::buddle, this));
	}
	virtual void stopSort() override {
		mIsStopThread.store(true);
	}

	virtual void oneStep() override {
		std::unique_lock<std::mutex> tMutex(mStepMutex);
		mStepCommend.notify_one();
	}

	virtual bool isStop() const override {
		return mIsStoped.load();
	}
	virtual void setSortType(SortType type) override { mSortType = type; }
	virtual SortType getSortType() const override { return mSortType; }
	virtual void setIntervalSelected(int mills) override { mIntervalSelected = mills; }
	virtual void setIntervalSwaped(int mills) override { mIntervalSwaped = mills; }
	virtual int getIntervalSelected() const override { return mIntervalSelected; }
	virtual int getIntervalSwaped() const override { return mIntervalSwaped; }
protected:
	virtual void buddle(){}

	SortDatas& mData;
	std::thread mThread;
	std::atomic<bool > mIsStopThread{ false };
	std::atomic<bool > mIsStoped{ true };
	SortType mSortType{ ST_AUTO };
	int mIntervalSelected{ 300 };
	int mIntervalSwaped{ 300 };

	std::mutex mStepMutex;//��
	std::condition_variable mStepCommend;//��������
};