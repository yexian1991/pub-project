#pragma once
#include "SortBase.h"

class QuickSort3: public SortBase
{
    Q_OBJECT
public:
	QuickSort3(SortDatas& data);
	~QuickSort3();

    //virtual void begin()override;
protected:
	virtual void buddle() override;

private:
	std::pair<int, int> __quickSortttt3(int left, int right);
	void __quickSort3( int left, int right);
};
