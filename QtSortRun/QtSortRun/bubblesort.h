#ifndef BUBBLESORT_H
#define BUBBLESORT_H
#include "SortBase.h"
#include<thread>

class BubbleSort: public SortBase
{
    Q_OBJECT
public:
    BubbleSort(SortDatas& data);
	~BubbleSort();

   // virtual void begin()override;
protected:
	virtual void buddle() override;
};

#endif // BUBBLESORT_H
