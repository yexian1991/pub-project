﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "bubblesort.h"
#include "selectsort.h"
#include "insertsort.h"
#include "xiersort.h"
#include "quicksort3.h"
#include<iostream>
#include <functional>
#include <qinputdialog.h>
#include <qtoolbar.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	initMenu();
	initCharts();
   setCentralWidget(mChartView);
}

void MainWindow::initMenu() {
	QMenuBar *menuBar = new QMenuBar(this);
	setMenuBar(menuBar);
	QMenu* sortMenu = menuBar->addMenu("Sort");

#define SELECT_SORT(actionName,actionDisplay,sortClass)\
	QAction* actionName = sortMenu->addAction(actionDisplay);\
	actionName->setIcon(QIcon(QString(":/icon/res/")+#actionName+".png"));\
	connect(actionName, &QAction::triggered, [=]() {\
		newData();\
		mChartView->chart()->setTitle(actionDisplay);\
		newSort(new sortClass(*mSortdata));\
	});

	SELECT_SORT(buddleAction, "Buddle Sort", BubbleSort);
	SELECT_SORT(selectAction, "Select Sort", SelectSort);
	SELECT_SORT(insertAction, "Insert Sort", InsertSort);
	SELECT_SORT(xierAction, "Xier Sort", XierSort);
	SELECT_SORT(quick3Action, "Quick3 Sort", QuickSort3);
#undef SELECT_SORT


	QMenu* editMenu = menuBar->addMenu("Edit");
#define RUN_COMMEND(actionName,actionDisplay)\
	QAction* actionName = editMenu->addAction(actionDisplay);\
	actionName->setIcon(QIcon(QString(":/icon/res/") + #actionName + ".png")); 

	RUN_COMMEND(autoAction, "Auto Sort");
	RUN_COMMEND(stepAction, "Step Sort");
	RUN_COMMEND(stopAction, "Stop Sort");
#undef RUN_COMMEND
	connect(autoAction, &QAction::triggered, [=]() {
		if (mSort != nullptr && mSort->isStop()){
			mSort->setSortType(abstractSort::SortType::ST_AUTO);
			mSort->begin();
		}
	});
	connect(stepAction, &QAction::triggered, [=]() {
		if (mSort == nullptr)
			return;
		if (mSort->isStop()){
			mSort->setSortType(abstractSort::SortType::ST_STEP);
			mSort->begin();
			return;
		}
		mSort->oneStep();
	});
	connect(stopAction, &QAction::triggered, [=]() {
		if (mSort != nullptr && mSort->isStop()==false){
			mSort->stopSort();
		}
	});

	QMenu* paramMenu	= menuBar->addMenu("Param");
	QAction* selectTimeAction	= paramMenu->addAction("time Before select");
	QAction* swapTimeAction = paramMenu->addAction("time Before swap");
	QAction* dataSizeAction		= paramMenu->addAction("data size");
	connect(selectTimeAction, &QAction::triggered, [=]() {
		bool ok{ false };
		auto ret = QInputDialog::getInt(this, tr("time Before select"),
			tr("time Before select:"), mIntervalSelected, 10, 1000, 1, &ok);
		if (ok) {
			mIntervalSelected = std::move(ret);
			if (mSort != nullptr)
				mSort->setIntervalSelected(mIntervalSelected);
		}
	});
	connect(swapTimeAction, &QAction::triggered, [=]() {
		bool ok{ false };
		auto ret = QInputDialog::getInt(this, tr("time Before swap"),
			tr("time Before swap:"), mIntervalSwaped, 10, 1000, 1, &ok);
		if (ok) {
			mIntervalSwaped = std::move(ret);
			if (mSort != nullptr)
				mSort->setIntervalSwaped(mIntervalSwaped);
		}
	});
	connect(dataSizeAction, &QAction::triggered, [=]() {
		bool ok;
		auto ret = QInputDialog::getInt(this, tr("data szie "),
			tr("data szie :"), 10, 20, 100, 1, &ok);
		if (ok) {
			mDataSize = std::move(ret);
			newData();
		}
	});


	QToolBar *toolBar = new QToolBar();
	addToolBar(toolBar);
	//将菜单某项放入工具栏
	toolBar->addAction(buddleAction);
	toolBar->addAction(selectAction);
	toolBar->addAction(insertAction);
	toolBar->addAction(xierAction);
	toolBar->addAction(quick3Action);

	toolBar->addSeparator();//分隔符
	toolBar->addAction(autoAction);
	toolBar->addAction(stepAction);
	toolBar->addAction(stopAction);

	toolBar->addSeparator();//分隔符
	toolBar->addAction(selectTimeAction);
	toolBar->addAction(swapTimeAction);
	toolBar->addAction(dataSizeAction);

	//一些属性
	toolBar->setAllowedAreas(Qt::TopToolBarArea);//可停靠区域
	toolBar->setMovable(false);//是否可移动
	toolBar->setFloatable(false);//是否浮动
}

void MainWindow::newData() {
	mBarSerise->clear();
	mSortdata = new SortDatas(mDataSize);
	for (int index = 0; index < mDataSize; ++index) {
		auto oneset = new QtCharts::QBarSet(QString::number(mSortdata->getData()[index]));
		oneset->setColor(INIT_COLOR);
		oneset->append(mSortdata->getData(	)[index] );
		mBarSerise->append(oneset);
	}
}

void MainWindow::newSort(abstractSort* sort) {

	if (mSort != nullptr) {
		mSort->stopSort();
		while (mSort->isStop() == false);
		disconnect(mSort, &abstractSort::compareSort, this, &MainWindow::reflushCompareData);
		disconnect(mSort, &abstractSort::swapSort, this, &MainWindow::reflushSwapData);
		delete mSort;
		mSort = nullptr;
	}
	mSort = sort;
	mSort->setIntervalSwaped(mIntervalSwaped);
	mSort->setIntervalSelected(mIntervalSelected);
	connect(mSort, &abstractSort::compareSort, this, &MainWindow::reflushCompareData, Qt::ConnectionType::QueuedConnection);
	connect(mSort, &abstractSort::swapSort, this, &MainWindow::reflushSwapData, Qt::ConnectionType::QueuedConnection);
}

void MainWindow::initCharts() {
	mBarSerise = new  QtCharts::QBarSeries();
	newData();

	QtCharts::QChart *m_chart = new QtCharts::QChart();
	m_chart->addSeries(mBarSerise); 
	mChartView = new QtCharts::QChartView(this);
	mChartView->setChart(m_chart);
	mChartView->show();
}

void MainWindow::reflushBar(const std::pair<int, int>& selIndex, const QColor& norColor, const QColor& selColor) {
	auto datas = mSortdata->getData();
	if (mBarSerise->count() != datas.size())
		return;

	for (int index = 0; index < mBarSerise->count(); ++index) {
		auto indexBar = mBarSerise->barSets().at(index);
		if (indexBar->at(0) != datas.at(index)) {
			indexBar->replace(0, datas.at(index));
			indexBar->setLabel(QString::number(indexBar->at(0)));
		}

		if ((selIndex.first != -1 && selIndex.first == index) ||
			(selIndex.second != -1 && selIndex.second == index))
			indexBar->setColor(selColor);
		else
			indexBar->setColor(norColor);
	}
}

void MainWindow::reflushSwapData() {
	reflushBar(mSortdata->getSwapIndex(), NORMAL_COLOR, SWAP_COLOR);
}

void MainWindow::reflushCompareData(){
	reflushBar(mSortdata->getCompareIndex(), NORMAL_COLOR, SELECT_COLOR);
}

MainWindow::~MainWindow()
{
	delete mSort;
	delete mSortdata;
    delete ui;
}

