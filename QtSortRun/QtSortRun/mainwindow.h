#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSet>
#include <QtCharts/QBarSeries>
#include <QtCharts/QChart>
#include "SortDatas.h"
#include "abstractSort.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
	void initMenu();
	void initCharts();
    void sortOver();
    void reflushCompareData();
    void reflushSwapData();
private:
	void newData();
	void newSort(abstractSort* sort);
	void reflushBar(const std::pair<int,int>& selIndex,const QColor& norColor,const QColor& selColor);
private:
    Ui::MainWindow *ui;
    QtCharts::QChartView *mChartView{nullptr};
    QtCharts::QBarSeries *mBarSerise{ nullptr };

    SortDatas* mSortdata{ nullptr };
	abstractSort* mSort{ nullptr };

	const QColor INIT_COLOR{ 125,125,125 };
	const QColor NORMAL_COLOR{ 0,255,0 };
	const QColor SELECT_COLOR{ 255,0,0 };
	const QColor SWAP_COLOR{ 0,0,255 };
	int mIntervalSelected{ 300 };
	int mIntervalSwaped{ 300 };
	int mDataSize{ 10 };
};
#endif // MAINWINDOW_H
